#!/usr/bin/make -f

.PHONY : all clean examples

all :
	gprbuild -P gpr/default.gpr

clean :
	gprclean -P gpr/default.gpr

examples : build/share/video/visi.bmp
	gprbuild -P gpr/example_sdl_minimal.gpr

build/share/video/% : examples/data/%
	mkdir -p $(@D)
	cp $< $@

.FORCE :