with Video.Pixels.Generic_Renderers;

package Video.Pixels.RGB.Renderers is

	package RGB565_Renderers is new Pixels.Generic_Renderers (
		RGB565_Pixel, RGB565_Array);

	package RGB888_Renderers is new Pixels.Generic_Renderers (
		RGB888_Pixel, RGB888_Array);

end Video.Pixels.RGB.Renderers;
