with Video.Integer_Geometry, Video.Colors, Video.Renderers;
use  Video.Integer_Geometry;
with Video.Pixels.Indexed;
use  Video.Pixels.Indexed;

generic
	type Pixel is private;
	type Pixel_Array is array (Integer range <>, Integer range <>) of Pixel;
	with function From_Color (Color : Colors.Color) return Pixel is <>;
	with function To_Color (Color : Pixel) return Colors.Color is <>;
package Video.Pixels.Generic_Renderers is

	type Renderer (Target : access Pixel_Array)
	is limited new Renderers.Renderer with private;

	overriding function Bounding_Box (S : Renderer) return Integer_Geometry.Box
	is (X => (S.Target'First (2), S.Target'Last (2) + 1), Y => (S.Target'First (1), S.Target'Last (1) + 1));

	-- New renderer

	overriding procedure Set_Color (
		Target : in out Renderer;
		C      : in     Colors.Color);

	overriding function Get_Color (
		Target : Renderer)
		return   Colors.Color;

	overriding function Pen_Extent (
		Target : Renderer)
		return   Box;

	overriding procedure Set_Clip_Box (
		Target : in out Renderer;
		B      : in     Box);

	overriding function Clip_Box (
		Target : Renderer)
		return   Box;

	overriding procedure Clear (
		Target : in out Renderer);

	overriding procedure Plot (
		Target : in out Renderer;
		A      : in     Point) with Inline;

	overriding procedure Fill_Rectangle (
		Target : in out Renderer;
		B      : in     Box);

	overriding procedure Line (
		Target : in out Renderer;
		A, B   : in     Point);

	overriding procedure Circle (
		Target : in out Renderer;
		Center : in     Point;
		Radius : in     Distance);

	overriding procedure Ellipse (
		Target : in out Renderer;
		Bounds : in     Box);

	overriding procedure Fill (
		Target : in out Renderer;
		Bounds : in     Box;
		Shape  : in     Index_1_Array;
		Offset : in     Point) is null; -- TODO: Implement

	overriding procedure Fill (
		Target : in out Renderer;
		Shape  : in     Index_1_Array;
		Offset : in     Point) is null; -- TODO: Implement

private
	type Renderer (Target : access Pixel_Array)
	is limited new Renderers.Renderer with record
		Pen  : Pixel := From_Color (Colors.Hex_6 (16#000000#));
	end record;

end Video.Pixels.Generic_Renderers;
