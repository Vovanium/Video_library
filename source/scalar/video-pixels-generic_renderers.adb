with Video.Pixels.Generic_Blits;

package body Video.Pixels.Generic_Renderers is

	package Blits is new Generic_Blits (
		Pixel => Pixel,
		Pixel_Array => Pixel_Array);

	--

	procedure Set_Color (
		Target : in out Renderer;
		C      : in     Colors.Color)
	is
	begin
		Target.Pen := From_Color (C);
	end Set_Color;

	function Get_Color (
		Target : Renderer)
		return   Colors.Color
	is (To_Color (Target.Pen));

	function Pen_Extent (
		Target : Renderer)
		return Box
	is ((0, 1), (0, 1));

	-- TODO: Clipping

	procedure Set_Clip_Box (
		Target : in out Renderer;
		B      : in     Box)
	is null;

	function Clip_Box (
		Target : Renderer)
		return   Box
	is (Full_Box);

	procedure Clear (
		Target : in out Renderer)
	is
	begin
		Target.Fill_Rectangle (Target.Bounding_Box);
	end Clear;

	procedure Plot (
		Target : in out Renderer;
		A      : in     Point)
	is
	begin
		Target.Target (A.Y, A.X) := Target.Pen;
	end Plot;

	procedure Fill_Rectangle (
		Target : in out Renderer;
		B      : in     Box)
	is
	begin
		for Y in B.Y.First .. B.Y.Last - 1 loop
			for X in B.X.First .. B.X.Last - 1 loop
				Target.Target (Y, X) := Target.Pen;
			end loop;
		end loop;
	end Fill_Rectangle;

	procedure Line (
		Target : in out Renderer;
		A, B   : in     Point)
	is
	begin
		Blits.Line (Target.Pen, Target.Target.all, A, B);
	end Line;

	procedure Circle (
		Target : in out Renderer;
		Center : in     Point;
		Radius : in     Distance)
	is
	begin
		Blits.Circle (Target.Pen, Target.Target.all, Center, Radius);
	end Circle;

	procedure Ellipse (
		Target : in out Renderer;
		Bounds : in     Box)
	is
	begin
		Blits.Ellipse (Target.Pen, Target.Target.all, Bounds);
	end Ellipse;

end Video.Pixels.Generic_Renderers;
