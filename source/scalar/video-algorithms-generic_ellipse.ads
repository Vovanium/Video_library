with Video.Integer_Geometry;
use  Video.Integer_Geometry;

generic
	type Raster (<>) is limited private;
	type Paint       is limited private;
	with procedure Write_Pixel (
		T : in out Raster;
		A : in     Point;
		C : in     Paint);
procedure Video.Algorithms.Generic_Ellipse (
	Target         : in out Raster;             -- Medium to paint on
	Bounds         : in     Box;                -- Ellipse boundaries
	Color          : in     Paint;              -- Painting data (like color, clipping area etc.)
	Clip           : in     Box := Full_Box);   -- Clipping area