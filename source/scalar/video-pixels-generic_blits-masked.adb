package body Video.Pixels.Generic_Blits.Masked is
	procedure Fill_Masked (
		Color  : in     Pixel;
		Mask   : in     Mask_Pixel_Array;
		Offset : in     Video.Integer_Geometry.Point;
		Target : in out Pixel_Array;
		Bounds : in     Video.Integer_Geometry.Box)
	is
	begin
		for Y in Bounds.Y.First .. Bounds.Y.Last - 1 loop
			for X in Bounds.X.First .. Bounds.X.Last - 1 loop
				if Mask (Y - Offset.Y, X - Offset.X) /= Mask_Value then
					Target (Y, X) := Color;
				end if;
			end loop;
		end loop;
	end Fill_Masked;

end Video.Pixels.Generic_Blits.Masked;