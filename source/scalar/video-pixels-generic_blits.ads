with Video.Integer_Geometry;
--
-- Generic software low level renderer
--
generic
	type Pixel is private;
	type Pixel_Array is array (Integer range <>, Integer range <>) of Pixel;
package Video.Pixels.Generic_Blits is

	procedure Fill_Rectangle (
		Color  : in     Pixel;
		Target : in out Pixel_Array;
		Bounds : in     Integer_Geometry.Box);

	procedure Line (
		Color  : in     Pixel;
		Target : in out Pixel_Array;
		A, B   : Integer_Geometry.Point);

	procedure Circle (
		Color  : in     Pixel;
		Target : in out Pixel_Array;
		Center : in     Integer_Geometry.Point;
		Radius : in     Natural);

	procedure Ellipse (
		Color  : in     Pixel;
		Target : in out Pixel_Array;
		Bounds : in     Integer_Geometry.Box);
	-- Note: Ellipse is actually drawn one pixel off Bounds, because it defines zero-thickness ellipse.

end Video.Pixels.Generic_Blits;
