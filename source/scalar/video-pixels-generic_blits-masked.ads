--
-- Generic software low level renderer for 1 bit masks
--
generic
	type Mask_Pixel is (<>);
	type Mask_Pixel_Array is array (Integer range <>, Integer range <>) of Mask_Pixel;
	Mask_Value : in Mask_Pixel := Mask_Pixel'First; -- Masking value (typically 0)
package Video.Pixels.Generic_Blits.Masked is

	procedure Fill_Masked (
		Color  : in     Pixel;
		Mask   : in     Mask_Pixel_Array;
		Offset : in     Video.Integer_Geometry.Point; -- Mask coordinate offset
		Target : in out Pixel_Array;
		Bounds : in     Video.Integer_Geometry.Box); -- Boundary rectangle in Target coords

end Video.Pixels.Generic_Blits.Masked;
