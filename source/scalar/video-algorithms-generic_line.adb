with Video.Integer_Geometry;
use type Video.Integer_Geometry.Point;

procedure Video.Algorithms.Generic_Line (
	Target         : in out Raster;
	A,      B      : in     Integer_Geometry.Point;
	A_Clip, B_Clip : in     Integer_Geometry.Point;
	Color          : in     Paint)
is
	D    : constant Integer_Geometry.Point := B - A; -- Direction vector
	S    : constant Integer_Geometry.Point := (
		X => (if D.X >= 0 then 1 else -1),
		Y => (if D.Y >= 0 then 1 else -1)); -- Signs of directions
	N, M : Integer; -- Denominator and numerator
	NC   : Integer; -- Number of steps
	E    : Integer;
	P    : Integer_Geometry.Point := A_Clip;
begin
	if abs D.X > abs D.Y then -- Nearly horizontal
		N := abs D.X;
		M := abs D.Y;
		E := -N / 2 + M * abs (A_Clip.X - A.X) - N * abs (A_Clip.Y - A.Y);
		 -- May need extended precision here
		NC := abs (B_Clip.X - A_Clip.X);
		for I in 0 .. NC loop
			Write_Pixel (Target, P, Color);
			E := E + M;
			if E >= 0 then
				E := E - N;
				P.Y := P.Y + S.Y;
			end if;
			P.X := P.X + S.X;
		end loop;
	else -- Nearly vertical
		N := abs D.Y;
		M := abs D.X;
		E := -N / 2 + M * abs (A_Clip.Y - A.Y) - N * abs (A_Clip.X - A.X);
		NC := abs (B_Clip.Y - A_Clip.Y);
		for I in 0 .. NC loop
			Write_Pixel (Target, P, Color);
			E := E + M;
			if E >= 0 then
				E := E - N;
				P.X := P.X + S.X;
			end if;
			P.Y := P.Y + S.Y;
		end loop;
	end if;

end Video.Algorithms.Generic_Line;
