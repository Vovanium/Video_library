with Video.Integer_Geometry;
generic
	type Raster (<>) is limited private;
	type Paint       is limited private;
	with procedure Write_Pixel (
		T : in out Raster;
		A : in     Integer_Geometry.Point;
		C : in     Paint);
procedure Video.Algorithms.Generic_Line (
	Target         : in out Raster;                  -- Medium to paint on
	A,      B      : in     Integer_Geometry.Point;  -- Virtual line ends (possibly out of clip area)
	A_Clip, B_Clip : in     Integer_Geometry.Point;  -- Actual first and last points to draw
	Color          : in     Paint);                  -- Painting data (like color, clipping area etc.)

-- A_Clip and B_Clip could be the same as A and B when no clipping is done
-- A_Clip is the closest to A and B_Clip is the closest to B to draw.
-- If order is not met algorighm could refuse to draw