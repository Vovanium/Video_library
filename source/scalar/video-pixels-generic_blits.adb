with Video.Integer_Geometry;
use  Video.Integer_Geometry;

with Video.Algorithms.Generic_Ellipse;
--with Ada.Text_IO;
--use  Ada.Text_IO;
--with Ada.Integer_Text_IO;
--use  Ada.Integer_Text_IO;

package body Video.Pixels.Generic_Blits is

	procedure Fill_Rectangle (
		Color  : in     Pixel;
		Target : in out Pixel_Array;
		Bounds : in     Video.Integer_Geometry.Box)
	is
	begin
		for Y in Bounds.Y.First .. Bounds.Y.Last - 1 loop
			for X in Bounds.X.First .. Bounds.X.Last - 1 loop
				Target (Y, X) := Color;
			end loop;
		end loop;
	end;

	procedure Line (
		Color  : in     Pixel;
		Target : in out Pixel_Array;
		A, B   : Integer_Geometry.Point)
	is
		D    : constant Integer_Geometry.Point := B - A;
		S    : constant Integer_Geometry.Point := (
			X => (if D.X >= 0 then 1 else -1),
			Y => (if D.Y >= 0 then 1 else -1));
		N, M : Integer;
		E    : Integer;
		P    : Integer_Geometry.Point := A;
	begin
		if abs D.X > abs D.Y then -- Nearly horizontal
			N := abs D.X;
			M := abs D.Y;
			E := -N / 2;
			for I in 1 .. N loop
				E := E + M;
				if E >= 0 then
					E := E - N;
					P.Y := P.Y + S.Y;
				end if;
				P.X := P.X + S.X;
				Target (P.Y, P.X) := Color;
			end loop;
		else -- Nearly vertical
			N := abs D.Y;
			M := abs D.X;
			E := -N / 2;
			for I in 1 .. N loop
				E := E + M;
				if E >= 0 then
					E := E - N;
					P.X := P.X + S.X;
				end if;
				P.Y := P.Y + S.Y;
				Target (P.Y, P.X) := Color;
			end loop;
		end if;
	end;

	procedure Circle (
		Color  : in     Pixel;
		Target : in out Pixel_Array;
		Center : in     Integer_Geometry.Point;
		Radius : in     Natural)
	is
		C : Integer := Radius; -- 'cosine' coordinate
		S : Integer := 0;      -- 'sine' coordinate
		D : Integer := Radius; -- (R**2 - S**2) - (C - 1/2)**2
	begin
		pragma Assert (D = Radius**2 - Radius * (Radius - 1));
		Target (Center.Y, Center.X + Radius) := Color;
		Target (Center.Y, Center.X - Radius) := Color;
		Target (Center.Y + Radius, Center.X) := Color;
		Target (Center.Y - Radius, Center.X) := Color;
		loop
			D := D - 2 * S - 1; -- S**2 - (S + 1)**2
			S := S + 1;
			pragma Assert (D = Radius**2 - S**2 - C * (C - 1));
			if D < 0 then
				C := C - 1;
				D := D + 2 * C; -- (C - 1/2)**2 - (C - 3/2)**2
			end if;
			pragma Assert (D = Radius**2 - S**2 - C * (C - 1));
			exit when C < S;
			Target (Center.Y + C, Center.X + S) := Color;
			Target (Center.Y + C, Center.X - S) := Color;
			Target (Center.Y - C, Center.X + S) := Color;
			Target (Center.Y - C, Center.X - S) := Color;
			Target (Center.Y + S, Center.X + C) := Color;
			Target (Center.Y + S, Center.X - C) := Color;
			Target (Center.Y - S, Center.X + C) := Color;
			Target (Center.Y - S, Center.X - C) := Color;
		end loop;
	end Circle;

	procedure Write_Pixel (
		Target : in out Pixel_Array;
		A      : in     Integer_Geometry.Point;
		Color  : in     Pixel)
	is
	begin
		Target (A.Y, A.X) := Color;
	end Write_Pixel;

	procedure Inner_Ellipse is new Video.Algorithms.Generic_Ellipse (
		Raster      => Pixel_Array,
		Paint       => Pixel,
		Write_Pixel => Write_Pixel);

	procedure Ellipse (
		Color  : in     Pixel;
		Target : in out Pixel_Array;
		Bounds : in     Integer_Geometry.Box)
	is
	begin
		Inner_Ellipse (Target, Bounds, Color);
	end Ellipse;

end Video.Pixels.Generic_Blits;
