#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_video.h>

/* from SDL */

const Uint32 SDL_Init_Everything = SDL_INIT_EVERYTHING;

/* from SDL_events */

const Uint32 SDL_Event_Quit       = SDL_QUIT;
const Uint32 SDL_Event_TextInput  = SDL_TEXTINPUT;
const Uint32 SDL_Event_MouseWheel = SDL_MOUSEWHEEL;
const Uint32 SDL_Event_UserEvent  = SDL_USEREVENT;

const size_t SDL_EventSize = sizeof (SDL_Event);

/* from SDL_pixels */

const Uint32 SDL_PixelFormat_RGB565 = SDL_PIXELFORMAT_RGB565;

/* from SDL_render */

const Sint32 SDL_TextureAccess_Static = SDL_TEXTUREACCESS_STATIC;

/* from SDL_video */

const int SDL_WindowPos_Centered = SDL_WINDOWPOS_CENTERED;

