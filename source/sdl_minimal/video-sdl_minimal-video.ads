with Interfaces.C.Strings;
use  Interfaces;

package Video.SDL_Minimal.Video is

	type Window is limited private;

	type Window_Ptr is access all Window with Convention => C;

	WindowPos_Centered : constant C.int
	with Import, External_Name => "SDL_WindowPos_Centered";

	type Window_Flags is new Unsigned_32;

	function CreateWindow (
		Title      : in C.Strings.chars_Ptr;
		X, Y, W, H : in C.int;
		Flags      : in Window_Flags)
		return          Window_Ptr
	with Import, Convention => C, External_Name => "SDL_CreateWindow";

	function CreateWindow (
		Title      : in String;
		X, Y, W, H : in C.int;
		Flags      : in Window_Flags)
		return          Window_Ptr;

	procedure DestroyWindow (Wnd : in Window_Ptr)
	with Import, Convention => C, External_Name => "SDL_DestroyWindow";

private

	type Window is null record;

end Video.SDL_Minimal.Video;
