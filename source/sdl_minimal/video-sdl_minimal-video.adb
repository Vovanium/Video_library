package body Video.SDL_Minimal.Video is

	function CreateWindow (
		Title      : in String;
		X, Y, W, H : in C.int;
		Flags      : in Window_Flags)
		return          Window_Ptr
	is
		T : C.Strings.chars_ptr := C.Strings.New_String (Title);
		R : Window_Ptr;
	begin
		R := CreateWindow (T, X, Y, W, H, Flags);
		C.Strings.Free (T);
		return R;
	end CreateWindow;

end Video.SDL_Minimal.Video;
