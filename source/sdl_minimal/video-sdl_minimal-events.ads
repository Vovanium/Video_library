with Interfaces.C;
use  Interfaces;
use type Interfaces.C.size_t;
with System;
use  System;

package Video.SDL_Minimal.Events is

	type EventType is new Unsigned_32;

	Quit       : constant EventType with Import, External_Name => "SDL_Event_Quit";
	MouseWheel : constant EventType with Import, External_Name => "SDL_Event_MouseWheel";
	User       : constant EventType with Import, External_Name => "SDL_Event_UserEvent";

	type CommonEvent is record
		Kind      : EventType;
		TimeStamp : Unsigned_32;
	end record with Convention => C;

	subtype TextEditingBuffer is Interfaces.C.char_array (0 .. 31);

	type TextEditingEvent is record
		Kind      : EventType;
		TimeStamp : Unsigned_32;
		WindowID  : Unsigned_32;
		Text      : TextEditingBuffer;
		Start     : Integer_32;
		Length    : Integer_32;
	end record with Convention => C;

	type MouseWheelEvent is record
		Kind      : EventType;
		TimeStamp : Unsigned_32;
		WindowID  : Unsigned_32;
		Which     : Unsigned_32;
		X         : Integer_32;
		Y         : Integer_32;
		Direction : Unsigned_32;
		PreciseX  : Float;
		PreciseY  : Float;
		MouseX    : Integer_32;
		MouseY    : Integer_32;
	end record with Convention => C;

	type UserEvent is record
		Kind      : EventType;
		TimeStamp : Unsigned_32;
		WindowID  : Unsigned_32;
		Code      : Integer_32;
		Data1     : Address;
		Data2     : Address;
	end record with Convention => C;

	type EventDis is (TextEditingE, MouseWheelE, UserE, OtherE);

	type Event (Kind : EventDis := OtherE) is record
		case Kind is
			when TextEditingE =>
				Edit   : TextEditingEvent;
			when MouseWheelE =>
				Wheel  : MouseWheelEvent;
			when UserE =>
				User   : UserEvent;
			when others =>
				Common : CommonEvent;
		end case;
	end record with Convention => C, Unchecked_Union;

	EventSize : constant C.size_t with Import, External_Name => "SDL_EventSize";

	AdaEventSize : Integer := Event'Max_Size_In_Storage_Elements;

	type Event_Ptr is access all Event with Convention => C;

	function PollEvent (Ev : in out Event) return C.int
	with Import, Convention => C, External_Name => "SDL_PollEvent";

	--

	pragma Assertion_Policy (Check);
	pragma Assert (Event'Max_Size_In_Storage_Elements = EventSize);

end Video.SDL_Minimal.Events;
