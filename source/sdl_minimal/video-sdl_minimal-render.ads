with Interfaces.C;
use  Interfaces;
use type Interfaces.C.int;
with System;
with Video.SDL_Minimal.Video;
use  Video.SDL_Minimal.Video;
with Video.SDL_Minimal.Pixels;
use  Video.SDL_Minimal.Pixels;
with Video.SDL_Minimal.Rect;
use  Video.SDL_Minimal.Rect;

package Video.SDL_Minimal.Render is

	type Renderer is limited private;
	type Renderer_Ptr is access all Renderer;

	type Texture is limited private;
	type Texture_Ptr is access all Texture;

	type RendererFlags is new Unsigned_32;

	type TextureAccess is new Integer_32;

	TextureAccess_Static : constant TextureAccess with Import, External_Name => "SDL_TextureAccess_Static";

	function CreateRenderer (
		Wnd   : Window_Ptr;
		Index : C.int := -1;
		Flags : RendererFlags := 0)
		return  Renderer_Ptr
	with Import, Convention => C, External_Name => "SDL_CreateRenderer";

	procedure DestroyRenderer (Rend : Renderer_Ptr)
	with Import, Convention => C, External_Name => "SDL_DestroyRenderer";

	function RenderClear (Rend : Renderer_Ptr) return C.int
	with Import, Convention => C, External_Name => "SDL_RenderClear";

	function RenderCopy (
		Rend    : Renderer_Ptr;
		Tx      : Texture_Ptr;
		SrcRect,
		DstRect : Rect_Ptr)
		return C.int
	with Import, Convention => C, External_Name => "SDL_RenderCopy";

	procedure RenderPresent (Rend : Renderer_Ptr)
	with Import, Convention => C, External_Name => "SDL_RenderPresent";

	function CreateTexture (
		Rend   : Renderer_Ptr;
		Format : PixelFormatEnum;
		Acs    : TextureAccess;
		W      : C.int;
		H      : C.int)
		return   Texture_Ptr
	with Import, Convention => C, External_Name => "SDL_CreateTexture";

	procedure DestroyTexture (Tx : Texture_Ptr)
	with Import, Convention => C, External_Name => "SDL_DestroyTexture";

	function UpdateTexture (
		Tx     : Texture_Ptr;
		Rect   : Rect_Ptr;
		Pixels : System.Address;
		Pitch  : C.int)
		return   C.int
	with Import, Convention => C, External_Name => "SDL_UpdateTexture";
private

	type Renderer is null record;

	type Texture is null record;

end Video.SDL_Minimal.Render;
