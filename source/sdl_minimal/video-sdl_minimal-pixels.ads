with Interfaces;
use  Interfaces;

package Video.SDL_Minimal.Pixels is

	type PixelFormatEnum is new Unsigned_32;

	PixelFormat_RGB565 : constant PixelFormatEnum with Import, External_Name => "SDL_PixelFormat_RGB565";

end Video.SDL_Minimal.Pixels;
