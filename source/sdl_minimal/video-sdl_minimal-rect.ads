with Interfaces.C;
use  Interfaces;

package Video.SDL_Minimal.Rect is

	type Rect is record
		X, Y : C.int;
		W, H : C.int;
	end record with Convention => C;

	type Rect_Ptr is access all Rect;

end Video.SDL_Minimal.Rect;
