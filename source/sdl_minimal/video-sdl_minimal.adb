
with Ada.Finalization;
with Interfaces.C;
use  Interfaces;
use type Interfaces.C.int;

package body Video.SDL_Minimal is

	type Init_Flags is new Unsigned_32;

	Init_Everything : constant Init_Flags
	with Import, External_Name => "SDL_Init_Everything";

	function Init (Flags : Init_Flags := Init_Everything) return C.int
	with Import, Convention => C, External_Name => "SDL_Init";

	procedure Quit with Import, Convention => C, External_Name => "SDL_Quit";

	type Finalizer is new Ada.Finalization.Controlled with null record;
	procedure Initialize (F : in out Finalizer);
	procedure Finalize (F : in out Finalizer);

	overriding procedure Initialize (F : in out Finalizer) is
	begin
		if Init /= 0 then
			raise Program_Error with "SDL refused to initialize";
		end if;
	end Initialize;

	overriding procedure Finalize (F : in out Finalizer) is
	begin
		Quit;
	end Finalize;

	F : Finalizer;

end Video.SDL_Minimal;
