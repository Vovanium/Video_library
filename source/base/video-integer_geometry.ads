package Video.Integer_Geometry with Pure is

	subtype Coordinate is Integer;

	type Coordinate_Product is range -(2 * Coordinate'First**2) .. 2 * Coordinate'Last**2;
	-- An extended range type for geometry computations

	subtype Distance is Coordinate'Base range 0 .. (
		if Coordinate'Last > Coordinate'Base'Last + Coordinate'First then
			Coordinate'Base'Last else Coordinate'Last - Coordinate'First);

	subtype Positive_Distance is Distance range 1 .. Distance'Last;

	type Point is record
		X, Y : Coordinate;
	end record;
	-- A 2D vector

	-- Note: As in many other graphics systems integral coordinate points
	-- between pixels. Pixel center is 0.5 away from it.
	--    Y
	--    ^
	--    |
	--  3 +--
	--    | * |   __ Pixel center at (1.5, 1.5)
	--  2 +---+  /
	--    | * | *
	--  1 +---+---+-
	--    | * | * | * |
	--  0 +---+---+---+--> X
	--    0   1   2   3
	-- When dealing with rasters and their index ranges offset by -1 appear at the Last index.

	-- Note on simple hairline graphics: It generally takes integer coordinates
	-- and use them to address adjacent pixel usually centered at (+0.5, +0.5).
	-- Clipping is (believed to be) performed correctly against pixel edges.

	function "+" (Q : Point) return Point is (Q);

	function "-" (Q : Point) return Point is (X => -Q.X, Y => -Q.Y);

	function "+" (P, Q : Point) return Point is (X => P.X + Q.X, Y => P.Y + Q.Y);

	function "-" (P, Q : Point) return Point is (X => P.X - Q.X, Y => P.Y - Q.Y);

	function "*" (K : Integer; P : Point) return Point is (X => K * P.X, Y => K * P.Y);
	-- Scale up vector by K

	function "*" (P : Point; K : Integer) return Point is (X => P.X * K, Y => P.Y * K);
	-- Scale up vector by K

	function "/" (P : Point; D : Integer) return Point is (X => P.X / D, Y => P.Y / D);
	-- Scale down a vector by D
	--

	--
	-- An Interval
	--

	type Interval is record
		First, Last : Coordinate;
	end record;
	-- A type like an Ada range (but a type)
	-- Values Coordinate'First at First and Coordinate'Last at Last
	-- are treated specially as 'no limit'

	function Empty_Interval return Interval is (Coordinate'Last, Coordinate'First);
	-- Interval with nothing in

	function Full_Interval return Interval is (Coordinate'First, Coordinate'Last);
	-- Interval covering entire Coordinate range
	-- Note: many operations on Full_Interval could cause overflow

	-- Note that transformations can cause overflow when applied to
	-- Empty and Full intervals. Library should treat these corner cases correctly.

	function Is_Empty (A : Interval) return Boolean is (A.First >= A.Last);
	-- Test if interval have nothing in

	function Is_Negative_Unlimited (A : Interval) return Boolean is (A.First <= Coordinate'First);
	-- Test if there's no lower limit

	function Is_Positive_Unlimited (A : Interval) return Boolean is (A.Last >= Coordinate'Last);
	-- Test if there's no upper limit

	subtype Nonempty_Interval is Interval with Dynamic_Predicate => not Is_Empty (Nonempty_Interval);
	-- A type for an interval that is not empty

	function Length (A : Interval) return Distance
	is (if Is_Empty (A) then 0 else A.Last - A.First);

	function Contains (A : Interval; X : Coordinate) return Boolean
	is (X in A.First .. A.Last);

	function Contains (Outer, Inner : Interval) return Boolean
	is (Is_Empty (Inner) or else (Outer.First <= Inner.First and Inner.Last <= Outer.Last));

	function Intersects (A, B : Interval) return Boolean
	is ((not Is_Empty (A)) and then (not Is_Empty (B)) and then (A.First <= B.Last and B.First <= A.Last));

	function "+" (A : Interval) return Interval is (A);
	-- Identity function

	function "-" (A : Interval) return Interval
	is (if Is_Empty (A) then Empty_Interval else (
		(if A.Last >= Coordinate'Last then Coordinate'First else -A.Last),
		(if A.First <= Coordinate'First then Coordinate'First else -A.First)));
	-- Negate inverval by flipping it over zero

	function "+" (A : Interval; B : Coordinate) return Interval
	is (if Is_Empty (A) then Empty_Interval else (
		(if A.First <= Coordinate'First then A.First else A.First + B),
		(if A.Last >= Coordinate'Last then A.Last else A.Last + B)));
	-- Translate interval

	function "+" (A : Coordinate; B : Interval) return Interval
	is (if Is_Empty (B) then Empty_Interval else (
		(if B.First <= Coordinate'First then B.First else A + B.First),
		(if B.Last >= Coordinate'Last then B.Last else A + B.Last)));
	-- Translate interval (other form)

	function "+" (A, B : Interval) return Interval
	is (if Is_Empty (A) or else Is_Empty (B) then Empty_Interval else (
		(if A.First <= Coordinate'First or B.First <= Coordinate'First then
			Coordinate'First else A.First + B.First),
		(if A.Last >= Coordinate'Last or B.Last >= Coordinate'Last then
			Coordinate'Last else A.Last + B.Last)));
	-- Minkowski sum
	-- The area sweeped by one interval when offset by values in other interval.

	function "-" (A : Interval; B : Coordinate) return Interval
	is (if Is_Empty (A) then Empty_Interval else (A.First - B, A.Last - B));
	-- Translate interval

	function "-" (A : Interval; B : Nonempty_Interval) return Interval
	is (if Is_Empty (A) then Empty_Interval else (
		(if A.First <= Coordinate'First then A.First else A.First - B.First),
		(if A.Last >= Coordinate'Last then A.Last else A.Last - B.Last)));
	-- Minkowski difference
	-- That is the result (if not empty) is the argument to minkowski sum with B to get A.
	-- Note: Minkowski difference is not the same as sum with negated interval

	function "and" (A, B : Interval) return Interval
	is (Coordinate'Max (A.First, B.First), Coordinate'Min (A.Last, B.Last));
	-- Intersection

	function "or" (A, B : Interval) return Interval
	is (if Is_Empty (A) then B
		elsif Is_Empty (B) then A
		else (Coordinate'Min (A.First, B.First), Coordinate'Max (A.Last, B.Last)));
	-- Minimal interval enclosing both arguments

	function Center (A : Nonempty_Interval) return Coordinate
	is (A.First / 2 + A.Last / 2 + (A.First rem 2 + A.Last rem 2) / 2);
	-- Central point (equidistant to both limits)

	function Center (A, B : Nonempty_Interval) return Coordinate
	is ((B.First + B.Last - A.First - A.Last) / 2);
	-- Offset to translate interval A to match center of interval B

	--
	-- Box
	--

	type Box is record
		X, Y : Interval;
	end record;
	-- An area enclosed by coordinate limits

	function Empty_Box return Box is (Empty_Interval, Empty_Interval);
	-- A box with no area

	function Full_Box return Box is (Full_Interval, Full_Interval);
	-- A "no clip" box

	function Is_Empty (B : Box) return Boolean is (Is_Empty (B.X) or else Is_Empty (B.Y));
	-- Test if a box is null

	subtype Nonempty_Box is Box with Dynamic_Predicate => not Is_Empty (Nonempty_Box);
	-- A type for a box that is not empty

	function Area (B : Box) return Coordinate_Product
	is (Coordinate_Product (Length (B.X)) * Coordinate_Product (Length (B.Y)));

	function Contains (B : Box; P : Point) return Boolean
	is (Contains (B.X, P.X) and then Contains (B.Y, P.Y));
	-- Test is point is inside or on the edge of a box

	function Contains (Outer, Inner : Box) return Boolean
	is (Contains (Outer.X, Inner.X) and then Contains (Outer.Y, Inner.Y));
	-- Test if Inner is inside Outer

	function Intersects (A, B : Box) return Boolean
	is (Intersects (A.X, B.X) and then Intersects (A.Y, B.Y));
	-- Test if two boxes have common area

	function "+" (B : Box) return Box is (B);
	-- Identity

	function "-" (B : Box) return Box
	is (if Is_Empty (B) then Empty_Box else (-B.X, -B.Y));
	-- Negation

	function "+" (B : Box; P : Point) return Box
	is (if Is_Empty (B) then Empty_Box else (B.X + P.X, B.Y + P.Y));
	-- Translate box

	function "+" (P : Point; B : Box) return Box
	is (if Is_Empty (B) then Empty_Box else (P.X + B.X, P.Y + B.Y));
	-- Translate box

	function "+" (A, B : Box) return Box
	is (if Is_Empty (A) or else Is_Empty (B) then Empty_Box else (A.X + B.X, A.Y + B.Y));
	-- Minkowski sum

	function "-" (B : Box; P : Point) return Box
	is (if Is_Empty (B) then Empty_Box else (B.X - P.X, B.Y - P.Y));
	-- Translate box

	function "-" (A : Box; B : Nonempty_Box) return Box
	is (if Is_Empty (A) then Empty_Box else (A.X - B.X, A.Y - B.Y));
	-- Minkowski difference
	-- That is the result (if not empty) is the argument to minkowski sum with B to get A
	-- Note: Minkowski difference is not the same as sum with negated box

	function "and" (A, B : Box) return Box
	is (X => A.X and B.X, Y => A.Y and B.Y);
	-- Intersection (like in sets)

	function "or" (A, B : Box) return Box
	is (X => A.X or B.X, Y => A.Y or B.Y);
	-- Minimal box enclosing both arguments
	-- Note: contrary to "and" it is not an set union

	function Center (P : Box) return Point
	is (X => Center (P.X), Y => Center (P.Y));

	function Center (A, B : Nonempty_Box) return Point
	is (X => Center (A.X, B.X), Y => Center (A.Y, B.Y));

end Video.Integer_Geometry;
