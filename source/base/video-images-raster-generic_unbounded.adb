package body Video.Images.Raster.Generic_Unbounded is

	overriding function Bounding_Box (Source : Image) return Box
	is
		Data : access Raster_Images.Array_Type := Source.Pixels.Reference;
		R    : Raster_Images.Array_Type renames Data.all;
	begin
		return (X => (R'First (2), R'Last (2) - 1),
			Y => (R'First (1), R'Last (1) - 1));
	end;

	overriding function Pixel (
		Source : Image;
		A      : Point)
		return   Raster_Images.Pixel_Type
	is
		Data : access Raster_Images.Array_Type := Source.Pixels.Reference;
	begin
		return Data.all (A.Y, A.X);
	end Pixel;

	overriding procedure Query_Raster (
		Source : in Image;
		Query  : not null access procedure (R : in Raster_Images.Array_Type))
	is
		Data : access Raster_Images.Array_Type := Source.Pixels.Reference;
	begin
		Query (Data.all);
	end Query_Raster;

	overriding procedure Process_Raster (
		Target  : in out Image;
		Process : not null access procedure (R : in out Raster_Images.Array_Type))
	is
		Data : access Raster_Images.Array_Type := Target.Pixels.Reference;
	begin
		Process (Data.all);
	end Process_Raster;

end Video.Images.Raster.Generic_Unbounded;
