with Ada.Numerics.Elementary_Functions;
use  Ada.Numerics.Elementary_Functions;

package body Video.Colorspaces.Float_RGB is

	package body sRGB is
		function To_sRGB (C_L : Float) return Float
		is (if C_L <= 0.0031308 then
			12.92 * C_L
		else
			1.055 * C_L**(1.0 / 2.4) - 0.055);

		function From_sRGB (C_S : Float) return Float
		is (if C_S <= 0.04045 then
			(1.0 / 12.92) * C_S
		else
			((1.0 / 1.055) * (C_S + 0.055))**2.4);
	end sRGB;

	package body With_Gamma is
		function From_Linear (C : Linear_Color) return Gamma_Color
		is (R => C.R**Inverse_Gamma, G => C.G**Inverse_Gamma, B => C.B**Inverse_Gamma, A => C.A);

		function To_Linear (C : Gamma_Color) return Linear_Color
		is (R => C.R**Gamma, G => C.G**Gamma, B => C.B**Gamma, A => C.A);
	end With_Gamma;

end Video.Colorspaces.Float_RGB;
