package body Video.Colors is

	function From_1_Bit (Value : Color_Component_1_Bit) return Color_Component
	is (Color_Component (Value * (2**8 - 1)));

	function From_4_Bit (Value : Color_Component_4_Bit) return Color_Component
	is (Color_Component (Value * (2**4 + 1)));

	function From_5_Bit (Value : Color_Component_5_Bit) return Color_Component
	is (Color_Component (Value * 2**3 + Value / 2**2));

	function From_6_Bit (Value : Color_Component_6_Bit) return Color_Component
	is (Color_Component (Value * 2**2 + Value / 2**4));

	function From_8_Bit (Value : Color_Component_8_Bit) return Color_Component
	is (Color_Component (Value));

	function To_1_Bit (Value : Color_Component) return Color_Component_1_Bit
	is (Color_Component_1_Bit (Value / 2**7));

	function To_4_Bit (Value : Color_Component) return Color_Component_4_Bit
	is (Color_Component_4_Bit (Value / 2**4));

	function To_5_Bit (Value : Color_Component) return Color_Component_5_Bit
	is (Color_Component_5_Bit (Value / 2**3));

	function To_6_Bit (Value : Color_Component) return Color_Component_6_Bit
	is (Color_Component_6_Bit (Value / 2**2));

	function To_8_Bit (Value : Color_Component) return Color_Component_8_Bit
	is (Color_Component_8_Bit (Value));

	function RGB_8 (
		R, G, B : Color_Component_8_Bit;
		A       : Color_Component_8_Bit := Color_Component_8_Bit'Last)
		return Color is
		((R => From_8_Bit (R), G => From_8_Bit (G), B => From_8_Bit (B), A => From_8_Bit (A)));


end Video.Colors;
