with Video.Integer_Geometry;

package Video.Texts with Pure is

	type Text_Extent is record
		Bounds  : Integer_Geometry.Box;   -- Bounding box of a rendered text
		Advance : Integer_Geometry.Point; -- caret movement
	end record;

	generic
		type Text_Type (<>) is private;
	package Generic_Writables is

		type Writable is limited interface;
		-- Basic type for any entity able to write a text

		function Extent (
			Target : Writable;
			Text   : Text_Type)
			return Text_Extent is abstract;
		-- Calculates text extent

		procedure Write (
			Target : in out Writable;
			A      : in     Integer_Geometry.Point;
			Text   : in     Text_Type) is null;
		-- Does the actual text rendering
		-- For the object doing only extent calculations leave this null

	end Generic_Writables;

	package Character_Writables is new Generic_Writables (Character);

	package Wide_Character_Writables is new Generic_Writables (Wide_Character);

	package Wide_Wide_Character_Writables is new Generic_Writables (Wide_Wide_Character);

	package String_Writables is new Generic_Writables (String);

	package Wide_String_Writables is new Generic_Writables (Wide_String);

	package Wide_Wide_String_Writables is new Generic_Writables (Wide_Wide_String);

end Video.Texts;
