with Video.Colors.Palettes;
use  Video.Colors.Palettes;

-- Handling of custom color maps (hardware, indexed image associated etc.)

package Video.Colors.Maps is

	type Constant_Color_Map is limited interface;
	-- A container-like type for custom color map, constant version

	function First_Color (Map : Constant_Color_Map) return Color_Index is abstract;
	-- First index in color map

	function Last_Color (Map : Constant_Color_Map) return Color_Index is abstract;
	-- Last index in color map

	function Map_Color (Map : Constant_Color_Map; Index : Color_Index) return Color is abstract;
	-- An element with specific index

	function To_Palette (Map : Constant_Color_Map) return Palette is abstract;
	-- Convert to plain palette

	type Color_Map is limited interface and Constant_Color_Map;
	-- A container-like type for custom color map, variable version

	procedure Replace_Color (
		Map   : in out Color_Map;
		Index : in     Color_Index;
		Value : in     Color) is abstract;
	-- Replace one color in map

	procedure From_Palette (
		Map    : in out Color_Map;
		Source : in     Palette) is abstract;
	-- Convert from plain palette. Palette should have same bounds as Map

	-- todo: Interator interface

end Video.Colors.Maps;