package body Video.Images.Raster.Generic_Fixed is

	overriding procedure Query_Raster (
		Source : in Image;
		Query  : not null access procedure (R : in Raster_Images.Array_Type))
	is
	begin
		Query (Source.Pixels);
	end Query_Raster;

	overriding procedure Process_Raster (
		Target  : in out Image;
		Process : not null access procedure (R : in out Raster_Images.Array_Type))
	is
	begin
		Process (Target.Pixels);
	end Process_Raster;

end Video.Images.Raster.Generic_Fixed;
