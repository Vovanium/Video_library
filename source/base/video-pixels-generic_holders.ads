--
-- Holders keep dynamically allocated rasters
--
with Ada.Finalization;
with Video.Integer_Geometry;
use  Video.Integer_Geometry;

generic
	type Pixel is private;
	type Pixel_Array is array (Integer range <>, Integer range <>) of Pixel;
	type Pixel_Array_Access is access Pixel_Array;
package Video.Pixels.Generic_Holders is

	type Holder is new Ada.Finalization.Controlled with private;

	function Bounding_Box (Source : Holder) return Box;

	procedure Set_Bounding_Box (
		Target : in out Holder;
		Bounds : in     Box);

	function Reference (Source : Holder) return access Pixel_Array;

	procedure Query_Raster (
		Source : in Holder;
		Query  : not null access procedure (R : in Pixel_Array));

	procedure Process_Raster (
		Target  : in out Holder;
		Process : not null access procedure (R : in out Pixel_Array));

	overriding procedure Adjust (Object : in out Holder);

	overriding procedure Finalize (Object : in out Holder);
private
	type Holder is new Ada.Finalization.Controlled with record
		Data : Pixel_Array_Access;
	end record;

end Video.Pixels.Generic_Holders;