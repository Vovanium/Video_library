with Video.Colors;
use  Video.Colors;

--
-- RGB pixels and arrays with Alpha
--
package Video.Pixels.ARGB is

	-- Pixel formats
	--  1 1 1 1 1 1
	--  5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
	-- +-+---------+---------+---------+
	-- |A|    R    |    G    |    B    | ARGB1555
	-- +-+---------+---------+---------+
	-- +-------+-------+-------+-------+
	-- |   A   |   R   |   G   |   B   | ARGB4444
	-- +-------+-------+-------+-------+
	--  3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
	--  1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
	-- +---------------+---------------+---------------+---------------+
	-- |       A       |       R       |       G       |       B       | ARGB8888 (aka ARGB32)
	-- +---------------+---------------+---------------+---------------+

	-- ARGB1555

	type ARGB1555_Pixel is record
		R : Color_Component_5_Bit;
		G : Color_Component_5_Bit;
		B : Color_Component_5_Bit;
		A : Color_Component_1_Bit;
	end record with Size => 16;
	for ARGB1555_Pixel use record
		R at 0 range 10 .. 14;
		G at 0 range  5 ..  9;
		B at 0 range  0 ..  4;
		A at 0 range 15 .. 15;
	end record;

	function From_Color (Color : Colors.Color) return ARGB1555_Pixel is (
		R => Colors.To_5_Bit (Color.R),
		G => Colors.To_5_Bit (Color.G),
		B => Colors.To_5_Bit (Color.B),
		A => Colors.To_1_Bit (Color.A)) with Inline;

	function To_Color (Pixel : ARGB1555_Pixel) return Color is (
		R => From_5_Bit (Pixel.R),
		G => From_5_Bit (Pixel.G),
		B => From_5_Bit (Pixel.B),
		A => From_1_Bit (Pixel.A)) with Inline;

	type ARGB1555_Array is array (Integer range <>, Integer range <>) of ARGB1555_Pixel;

	-- ARGB4444

	type ARGB4444_Pixel is record
		R : Color_Component_4_Bit;
		G : Color_Component_4_Bit;
		B : Color_Component_4_Bit;
		A : Color_Component_4_Bit;
	end record with Size => 16;
	for ARGB4444_Pixel use record
		R at 0 range  8 .. 11;
		G at 0 range  4 ..  7;
		B at 0 range  0 ..  3;
		A at 0 range 12 .. 15;
	end record;

	function From_Color (Color : Colors.Color) return ARGB4444_Pixel is (
		R => Colors.To_4_Bit (Color.R),
		G => Colors.To_4_Bit (Color.G),
		B => Colors.To_4_Bit (Color.B),
		A => Colors.To_4_Bit (Color.A)) with Inline;

	function To_Color (Pixel : ARGB4444_Pixel) return Color is (
		R => From_4_Bit (Pixel.R),
		G => From_4_Bit (Pixel.G),
		B => From_4_Bit (Pixel.B),
		A => From_4_Bit (Pixel.A)) with Inline;

	type ARGB4444_Array is array (Integer range <>, Integer range <>) of ARGB4444_Pixel;

	-- ARGB8888

	type ARGB8888_Pixel is record
		R : Color_Component_8_Bit;
		G : Color_Component_8_Bit;
		B : Color_Component_8_Bit;
		A : Color_Component_8_Bit;
	end record with Size => 32;
	for ARGB8888_Pixel use record
		R at 0 range 16 .. 23;
		G at 0 range  8 .. 15;
		B at 0 range  0 ..  7;
		A at 0 range 24 .. 31;
	end record;

	function From_Color (Color : Colors.Color) return ARGB8888_Pixel is (
		R => Colors.To_8_Bit (Color.R),
		G => Colors.To_8_Bit (Color.G),
		B => Colors.To_8_Bit (Color.B),
		A => Colors.To_8_Bit (Color.A)) with Inline;

	function To_Color (Pixel : ARGB8888_Pixel) return Color is (
		R => From_8_Bit (Pixel.R),
		G => From_8_Bit (Pixel.G),
		B => From_8_Bit (Pixel.B),
		A => From_8_Bit (Pixel.A)) with Inline;

	type ARGB8888_Array is array (Integer range <>, Integer range <>) of ARGB8888_Pixel;

end Video.Pixels.ARGB;
