--
-- Instantiations of Generic_Raster_Images with RGB types
--
with Video.Pixels.ARGB;
use  Video.Pixels.ARGB;

package Video.Images.Raster.ARGB is

	type ARGB_Raster_Image is limited interface and Raster_Image;

	package ARGB1555 is new Generic_Raster (
		Pixel_Type => ARGB1555_Pixel,
		Array_Type => ARGB1555_Array,
		Parent     => ARGB_Raster_Image);

	package ARGB4444 is new Generic_Raster (
		Pixel_Type => ARGB4444_Pixel,
		Array_Type => ARGB4444_Array,
		Parent     => ARGB_Raster_Image);

	package ARGB8888 is new Generic_Raster (
		Pixel_Type => ARGB8888_Pixel,
		Array_Type => ARGB8888_Array,
		Parent     => ARGB_Raster_Image);

end Video.Images.Raster.ARGB;
