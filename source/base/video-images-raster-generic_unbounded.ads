--
-- Images with raster that can be changed in runtime
--
with Ada.Containers.Indefinite_Holders;
with Video.Pixels.Generic_Holders;

generic
	with package Raster_Images is new Generic_Raster (<>);
	type Parent is abstract tagged private;
package Video.Images.Raster.Generic_Unbounded is

	type Image is abstract new Parent and Raster_Images.Image with private;

	overriding function Bounding_Box (Source : Image) return Box;

	overriding function Pixel (
		Source : Image;
		A      : Point)
		return   Raster_Images.Pixel_Type;

	overriding procedure Query_Raster (
		Source : in Image;
		Query  : not null access procedure (R : in Raster_Images.Array_Type));

	overriding procedure Process_Raster (
		Target  : in out Image;
		Process : not null access procedure (R : in out Raster_Images.Array_Type));

private
	type Array_Access is access Raster_Images.Array_Type;
	package Raster_Holders is new Video.Pixels.Generic_Holders (
		Pixel              => Raster_Images.Pixel_Type,
		Pixel_Array        => Raster_Images.Array_Type,
		Pixel_Array_Access => Array_Access);

	type Image is abstract new Parent and Raster_Images.Image with record
		Pixels : Raster_Holders.Holder;
	end record;

end Video.Images.Raster.Generic_Unbounded;
