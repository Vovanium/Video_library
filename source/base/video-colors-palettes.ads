package Video.Colors.Palettes is

	subtype Color_Index is Natural;

	type Palette is array (Color_Index range <>) of Color;
	Empty_Palette : constant Palette (1 .. 0) := (others => <>);

end Video.Colors.Palettes;
