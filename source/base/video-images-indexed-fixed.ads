with Video.Images.Raster.Generic_Fixed;
with Video.Colors.Maps.Generic_Fixed;
with Video.Colors.Maps.Shared_Palettes;

package Video.Images.Indexed.Fixed is

	generic
		with package Base is new Indexed_Base (<>);
	package Generic_Fixed is

		package With_Own_Palette is new Raster.Generic_Fixed (
			Raster_Images => Base.Raster,
			Parent        => Base.Own.Indexed_Image_Base);

		type Image_with_Own_Palette is new With_Own_Palette.Image with null record;

		--

		package With_Shared_Palette is new Raster.Generic_Fixed (
			Raster_Images => Base.Raster,
			Parent        => Base.Shared.Indexed_Image_Base);

		type Image_with_Shared_Palette is new With_Shared_Palette.Image with null record;

	end Generic_Fixed;

	--package Fixed_Index_1 is new Generic_Fixed (
	--	Base => Index_1_Base);

	--package Fixed_Index_4 is new Generic_Fixed (
	--	Base => Index_4_Base);

	-- Above does not compile due to bug in GNAT

	package Fixed_Index_8 is new Generic_Fixed (
		Base => Index_8);

end Video.Images.Indexed.Fixed;
