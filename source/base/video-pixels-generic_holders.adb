with Ada.Unchecked_Deallocation;

package body Video.Pixels.Generic_Holders is

	function Bounding_Box (Source : Holder) return Box is (
		if Source.Data /= null then (
			X => (Source.Data.all'First (2), Source.Data.all'Last (2) + 1),
			Y => (Source.Data.all'First (1), Source.Data.all'Last (1) + 1))
		else
			Empty_Box);

	function Reference (Source : Holder) return access Pixel_Array
	is (Source.Data);

	procedure Query_Raster (
		Source : in Holder;
		Query  : not null access procedure (R : in Pixel_Array))
	is
	begin
		if Source.Data /= null then
			Query (Source.Data.all);
		end if;
	end Query_Raster;

	procedure Process_Raster (
		Target  : in out Holder;
		Process : not null access procedure (R : in out Pixel_Array))
	is
	begin
		if Target.Data /= null then
			Process (Target.Data.all);
		end if;
	end Process_Raster;

	--

	procedure Deallocate is new Ada.Unchecked_Deallocation (
		Object => Pixel_Array,
		Name   => Pixel_Array_Access);

	procedure Allocate (
		Target : in out Holder;
		Bounds : in     Box)
	with Pre => (Target.Data = null)
	is
	begin
		if not Is_Empty (Bounds) then
			Target.Data := new Pixel_Array (
				Bounds.Y.First .. Bounds.Y.Last - 1,
				Bounds.X.First .. Bounds.X.Last - 1);
		end if;
	end Allocate;

	procedure Set_Bounding_Box (
		Target : in out Holder;
		Bounds : in     Box)
	is
	begin
		Deallocate (Target.Data);
		Allocate (Target, Bounds);
	end Set_Bounding_Box;

	overriding procedure Adjust (Object : in out Holder) is
	begin
		if Object.Data /= null then
			Object.Data := new Pixel_Array'(Object.Data.all); -- make a copy
		end if;
	end Adjust;

	overriding procedure Finalize (Object : in out Holder) is
	begin
		Deallocate (Object.Data);
	end Finalize;

end Video.Pixels.Generic_Holders;
