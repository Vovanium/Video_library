--
-- A simple implementation for monospaced raster fonts
--
with Video.Integer_Geometry;
use  Video.Integer_Geometry;

generic
	Width, Height : Positive_Distance;
	type Pixel is private;
	type Pixel_Array is array (Coordinate range <>, Coordinate range <>) of Pixel;
	type Character_Type is (<>);
package Video.Texts.Fonts.Raster.Glyph_Arrays is

	subtype Glyph is Pixel_Array (0 .. Height - 1, 0 .. Width - 1);

	type Glyph_Array is array (Character_Type range <>) of Glyph;

end Video.Texts.Fonts.Raster.Glyph_Arrays;
