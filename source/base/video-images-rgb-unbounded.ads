with Video.Images.Raster.Generic_Unbounded;

package Video.Images.RGB.Unbounded is

	generic
		with package Base is new RGB_Base (<>);
	package Generic_Unbounded is

		package Unbounded_Base is new Raster.Generic_Unbounded (
			Raster_Images => Base.Raster,
			Parent        => Base.RGB_Image_Base);
		-- Generic instantiation

		type Image is new Unbounded_Base.Image with null record;
		-- Actual Unbounded RGB Image

	end Generic_Unbounded;

	package Unbounded_RGB565 is new Generic_Unbounded (
		Base => RGB565);

	package Unbounded_RGB888 is new Generic_Unbounded (
		Base => RGB888);

end Video.Images.RGB.Unbounded;
