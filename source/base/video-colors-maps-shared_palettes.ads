-- Holding access to shared palette
package Video.Colors.Maps.Shared_Palettes is

	type Constant_Shared_Palette is new Constant_Color_Map with record
		Colors : access constant Palette;
	end record;

	overriding function First_Color (Map : Constant_Shared_Palette) return Color_Index
	is (Map.Colors.all'First);

	overriding function Last_Color (Map : Constant_Shared_Palette) return Color_Index
	is (Map.Colors.all'Last);

	overriding function Map_Color (Map : Constant_Shared_Palette; Index : Color_Index) return Color
	is (Map.Colors.all (Index));

	overriding function To_Palette (Map : Constant_Shared_Palette) return Palette
	is (Map.Colors.all);

end Video.Colors.Maps.Shared_Palettes;