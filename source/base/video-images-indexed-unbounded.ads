with Video.Colors.Maps.Generic_Fixed;
with Video.Colors.Maps.Shared_Palettes;
with Video.Images.Raster.Generic_Unbounded;

package Video.Images.Indexed.Unbounded is

	generic
		with package Base is new Indexed_Base (<>);
	package Generic_Unbounded is

		package With_Own_Palette is new Raster.Generic_Unbounded (
			Raster_Images => Base.Raster,
			Parent        => Base.Own.Indexed_Image_Base);

		type Image_with_Own_Palette is new With_Own_Palette.Image with null record;

		--

		package With_Shared_Palette is new Raster.Generic_Unbounded (
			Raster_Images => Base.Raster,
			Parent        => Base.Shared.Indexed_Image_Base);

		type Image_with_Shared_Palette is new With_Shared_Palette.Image with null record;

	end Generic_Unbounded;

	package Unbounded_Index_1 is new Generic_Unbounded (
		Base => Index_1);

	package Unbounded_Index_4 is new Generic_Unbounded (
		Base => Index_4);

	package Unbounded_Index_8 is new Generic_Unbounded (
		Base => Index_8);

end Video.Images.Indexed.Unbounded;
