
--
-- Simple affine transformations, that keep scale and ortogonality
package Video.Integer_Geometry.Unitary_Transformations is

	subtype Unitary_Coordinate is Coordinate range -1 .. 1;

	type Unitary_Vector is record
		X, Y : Unitary_Coordinate;
	end record;

	function "+" (P, Q : Unitary_Vector) return Unitary_Vector is
	(X => P.X + Q.X, Y => P.Y + Q.Y);

	function "*" (P : Unitary_Vector; K : Unitary_Coordinate) return Unitary_Vector is
	(X => P.X * K, Y => P.Y * K);

	function To_Point (P : Unitary_Vector) return Point is (X => P.X, Y => P.Y);

	type Transformation is record
		X : Unitary_Vector := (1, 0); -- New direction of original X coordinate axis
		Y : Unitary_Vector := (0, 1); -- New direction of original Y coordinate axis
		B : Point          := (0, 0); -- Origin offset
	end record;

	-- Note: transformation matrix is stored by-column.
	--       || m_xx m_xy m_xt ||     ((m_xx, m_yx),
	--       || m_yx m_yy m_yt || ->   (m_xy, m_yy),
	--       ||  0    0    1   ||      (m_xt, m_yt))

	No_Transformation : constant Transformation :=  ((1,  0),  (0,  1), (0, 0));
	Flip_X            : constant Transformation := ((-1,  0),  (0,  1), (0, 0));
	Flip_Y            : constant Transformation :=  ((1,  0),  (0, -1), (0, 0));
	Exchange_X_Y      : constant Transformation :=  ((0,  1),  (1,  0), (0, 0));
	Rotate_90         : constant Transformation :=  ((0,  1), (-1,  0), (0, 0));
	Rotate_180        : constant Transformation := ((-1,  0),  (0, -1), (0, 0));
	Rotate_270        : constant Transformation :=  ((0, -1),  (1,  0), (0, 0));
	function Translate (Offset : Point) return Transformation is ((1, 0), (0, 1), Offset);

	-- Apply transformation to point
	function "*" (M : Transformation; P : Point) return Point is
	(To_Point (M.X) * P.X + To_Point (M.Y) * P.Y + M.B);

	-- Combining transformations:
	-- Result is a transformation M applied after transformation N
	function "*" (M, N : Transformation) return Transformation is (
		X => M.X * N.X.X + M.Y * N.X.Y,
		Y => M.X * N.Y.X + M.Y * N.Y.Y,
		B => M * N.B);

	-- Moves center of transformation from origin to an arbitrary point
	-- Works in flips and rotations
	function Transform_About (M : Transformation; P : Point) return Transformation is
	(Translate (P) * M * Translate (-P));

end Video.Integer_Geometry.Unitary_Transformations;
