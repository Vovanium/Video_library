package body Video.Colors.Maps.Fixed is

	procedure Replace_Color (
		Map   : in out Fixed_Color_Map;
		Index : in     Color_Index;
		Value : in     Color)
	is
	begin
		Map.Colors (Index) := Value;
	end Replace_Color;

	procedure From_Palette (
		Map    : in out Fixed_Color_Map;
		Source : in     Palette)
	is
	begin
		Map.Colors := Source;
	end From_Palette;

end Video.Colors.Maps.Fixed;
