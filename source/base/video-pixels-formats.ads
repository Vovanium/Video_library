package Video.Pixels.Formats with Pure is
	subtype Component_Offset is Integer range 0 .. 2**8 - 1;

	type Component_Range is record
		First : Component_Offset;
		Last  : Component_Offset;
	end record;

	No_Component : constant Component_Range := (1, 0);

	type Color_Mode is (
		Index,
		ARGB);

	type Pixel_Format (Mode : Color_Mode) is record
		Pitch : Component_Offset;
		case Mode is
		when Index =>
			null;
		when ARGB =>
			R, G, B : Component_Range;
			A       : Component_Range := No_Component;
		end case;
	end record;

	No_Format : constant Pixel_Format := (Mode => Index, Pitch => 0);

end Video.Pixels.Formats;
