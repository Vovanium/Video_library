with Video.Images.Raster.Generic_Fixed;

package Video.Images.RGB.Fixed is

	generic
		with package Base is new RGB_Base (<>);
	package Generic_Fixed is

		package Fixed_Base is new Raster.Generic_Fixed (
			Raster_Images => Base.Raster,
			Parent        => Base.RGB_Image_Base);
		-- Generic instantiation

		type Image is new Fixed_Base.Image with null record;
		-- Actual Unbounded RGB Image

	end Generic_Fixed;

	package Fixed_RGB888 is new Generic_Fixed (
		Base          => RGB888);

end Video.Images.RGB.Fixed;
