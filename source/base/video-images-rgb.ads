--
-- Instantiations of Generic_Raster_Images with RGB types
--
with Video.Pixels.RGB;
use  Video.Pixels.RGB;

package Video.Images.RGB is

	type RGB_Image is limited interface and Raster_Image;

	generic
		type Pixel_Type is private;
		type Array_Type is array (Integer range <>, Integer range <>) of Pixel_Type;
		with function To_Color (Pixel : Pixel_Type) return Color is <>;
	package RGB_Base is

		package Raster is new Generic_Raster (
			Pixel_Type => Pixel_Type,
			Array_Type => Array_Type,
			Parent     => RGB_Image);

		subtype Image is Raster.Image;

		type RGB_Image_Base is abstract new Image with null record;

		function Pixel (
			Source : RGB_Image_Base;
			A      : Point)
			return   Color
		is (To_Color (Pixel_Type'(Raster.Image'Class (Source).Pixel (A))));

	end RGB_Base;

	package RGB565 is new RGB_Base (
		Pixel_Type => RGB565_Pixel,
		Array_Type => RGB565_Array);

	package RGB888 is new RGB_Base (
		Pixel_Type => RGB888_Pixel,
		Array_Type => RGB888_Array);

end Video.Images.RGB;
