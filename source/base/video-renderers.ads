with Video.Integer_Geometry, Video.Colors, Video.Pixels.Indexed;
use  Video.Integer_Geometry, Video.Colors, Video.Pixels.Indexed;
package Video.Renderers with Pure is

	type Renderer is limited interface;
	-- Stateful full-featured rendering interface

	-- Common subprograms

	function Bounding_Box (
		Target : Renderer)
		return   Box is abstract;
	-- Coordinate limits of a surface

	procedure Sync (
		Target : in out Renderer) is null;
	-- Make painting available to outer world by e.g.
	-- - pushing data to the attached graphic device,
	-- - waiting for painting process to be complete.
	--
	-- Alternatively synchronization should be done
	-- on the destruction of the renderable object

	-- Defining rendering context

	procedure Set_Color (
		Target : in out Renderer;
		C      : in     Color) is abstract;
	-- Set color of a pen

	function Get_Color (
		Target : Renderer)
		return   Color is abstract;
	-- Query current rendering color.
	-- Note: The function may return a value that is different to
	-- what is set because it may be stored converted to a device
	-- dependent format with loss of precision.

	function Pen_Extent (
		Target : Renderer)
		return   Box is abstract;
	-- Query size of a current pen
	-- Function returns a bounding box of current drawing pen relative to its position
	-- that is if point is plotted at p, then its extent will be p + Pen_Extent
	-- It is usable to compute cooridnates of shapes that fits inside specified area
	-- by subtracting pen extent, or embraces an area by adding it.
	-- Some renderers may be inaccurate so real pen may differ in size and offset a bit.
	-- It can be of zero size if renderer treats lines and plots as 'zero width'.

	procedure Set_Clip_Box (
		Target : in out Renderer;
		B      : in     Box) is abstract;
	-- Define clipping box

	function Clip_Box (
		Target : Renderer)
		return   Box is abstract;
	-- Query clipping box

	-- Area fills
	-- Usually use fill pattrn to render

	procedure Clear (
		Target : in out Renderer) is abstract;
	-- Fill all surface (or clip box) with fill pattern

	procedure Fill_Rectangle (
		Target : in out Renderer;
		B      : in     Box) is abstract;
	-- Fill a rectangle with fill pattern

	-- Line draw

	procedure Plot (
		Target : in out Renderer;
		A      : in     Point) is abstract;
	-- Draw a single point with pen

	procedure Line (
		Target : in out Renderer;
		A, B   : in     Point) is abstract;
	-- Draw straight line with pen

	procedure Circle (
		Target : in out Renderer;
		Center : in     Point;
		Radius : in     Distance) is abstract;

	procedure Ellipse (
		Target : in out Renderer;
		Bounds : in     Box) is abstract;
	-- Draw an ellipse

	-- Arbitrary shape draw

	procedure Fill (
		Target : in out Renderer;
		Bounds : in     Box;
		Shape  : in     Index_1_Array;
		Offset : in     Point) is abstract;
	-- Draw a shape defined by bitmap

	procedure Fill (
		Target : in out Renderer;
		Shape  : in     Index_1_Array;
		Offset : in     Point) is abstract;
	-- Draw a shape defined by bitmap

end Video.Renderers;
