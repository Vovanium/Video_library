with Video.Integer_Geometry;

package Video.Texts.Fonts.Raster is

	type Mapped_Glyph is record
		Encoding   : Wide_Wide_Character;
		Advance_X  : Integer;
		Bounds     : Integer_Geometry.Box; -- a bounding box of a glyph relative to its origin
		Map        : Integer_Geometry.Point; -- a position of a glyph corner (X.First, Y.First) on a the map
	end record;

	type Mapped_Glyph_Array is array (Positive range <>) of Mapped_Glyph;

	type Constant_Mapped_Glyph_Array_Access is access constant Mapped_Glyph_Array;

	generic
		type Pixel is private;
		type Pixel_Array is array (Integer range <>, Integer range <>) of Pixel;
	package Mapped is
		type Constant_Pixel_Array_Access is access constant Pixel_Array;

		type Mapped_Raster_Font is record
			Map   : Constant_Mapped_Glyph_Array_Access;
			Image : Constant_Pixel_Array_Access;
		end record;
	end Mapped;

end Video.Texts.Fonts.Raster;
