--
-- Images with fixed raster not using any dynamic storage allocation
--

generic
	with package Raster_Images is new Generic_Raster (<>);
	type Parent is abstract tagged private;
package Video.Images.Raster.Generic_Fixed is

	type Image (X_First, X_Last, Y_First, Y_Last : Integer)
	is abstract new Parent and Raster_Images.Image with record
		Pixels : Raster_Images.Array_Type (Y_First .. Y_Last, X_First .. Y_Last);
	end record;

	overriding function Bounding_Box (Source : Image) return Box
	is ((Source.X_First, Source.X_Last - 1), (Source.Y_First, Source.Y_Last - 1));

	overriding function Pixel (
		Source : Image;
		A      : Point)
		return   Raster_Images.Pixel_Type
	is (Source.Pixels (A.Y, A.X));

	overriding procedure Query_Raster (
		Source : in Image;
		Query  : not null access procedure (R : in Raster_Images.Array_Type));

	overriding procedure Process_Raster (
		Target  : in out Image;
		Process : not null access procedure (R : in out Raster_Images.Array_Type));

end Video.Images.Raster.Generic_Fixed;
