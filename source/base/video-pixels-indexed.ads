package Video.Pixels.Indexed with Pure is

	type Index_1_Pixel is mod 2;
	type Index_2_Pixel is mod 4;
	type Index_4_Pixel is mod 16;
	type Index_8_Pixel is mod 256;

	type Index_1_Array is array (Integer range <>, Integer range <>) of Index_1_Pixel with Pack;
	type Index_2_Array is array (Integer range <>, Integer range <>) of Index_2_Pixel with Pack;
	type Index_4_Array is array (Integer range <>, Integer range <>) of Index_4_Pixel with Pack;
	type Index_8_Array is array (Integer range <>, Integer range <>) of Index_8_Pixel with Pack;

end Video.Pixels.Indexed;