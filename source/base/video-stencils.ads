with Video.Integer_Geometry, Video.Colors;
use  Video.Integer_Geometry, Video.Colors;

package Video.Stencils is

	type Stencil is interface;

	function Bounding_Box (
		Source  : Stencil)
		return    Box is abstract;
	-- Stencil bounds in native (whatever it means) resolution

	function Pixel (
		Source  : Stencil;
		A       : Point)
		return    Opacity is abstract;

	type Raster_Stencil is interface and Stencil;

	generic
		type Pixel_Type is private;
	package Generic_Raster is
		type Stencil is interface and Raster_Stencil;

		function Pixel (
			Source : Stencil;
			A      : Point)
			return Pixel_Type is abstract;
	end Generic_Raster;

end Video.Stencils;