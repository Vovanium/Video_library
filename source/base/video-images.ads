--
-- Handling for images, say sources of graphic information.
--
with Video.Integer_Geometry, Video.Colors;
use  Video.Integer_Geometry, Video.Colors;

package Video.Images with Pure is

	type Image is limited interface;
	-- The interface for image
	-- (Not file format, but vector/raster, various pixel types etc.)

	function Bounding_Box (
		Source  : Image)
		return    Box is abstract;
	-- Image bounds in native (whatever it means) resolution

	function Pixel (
		Source   : Image;
		A        : Point)
		return     Color is abstract;
	-- Color of an individual pixel in native resolution
	-- Generally it is an average color for (X .. X + 1, T .. Y + 1) square

	type Raster_Image is limited interface and Image;
	-- Root type for raster images

	-- A helper generic package to keep types with the same format togeter

	generic
		type Pixel_Type is private;
		type Array_Type is array (Integer range <>, Integer range <>) of Pixel_Type;
		type Parent     is limited interface and Raster_Image;
	package Generic_Raster is

		-- Alias them so external code can use them

		-- Passing Parent allow added level of hierarchy
		-- Indexed, RGB etc images are grouped

		type Image is limited interface and Parent;
		-- A raster image those pixel type is defined

		--function Map_Color (
		--	Source : Image;
		--	Pixel : Pixel_Type) return Color is abstract;

		function Pixel (
			Source : Image;
			A      : Point) return Pixel_Type is abstract;
		-- Get a single pixel

		procedure Query_Raster (
			Source : in     Image;
			Query  : not null access procedure (R : in     Array_Type)) is abstract;
		-- Query actual raster (maybe by chunks, seee below)

		procedure Process_Raster (
			Target  : in out Image;
			Process : not null access procedure (R : in out Array_Type)) is null;
		-- Modify actual raster. Might be null for images that are not modifiable.
		-- Non modifiable image could be declared constant so this call will not compile

		-- If Raster is not used as the main image storage, these subprograms
		-- may split the data into chunks and call Query/Process several times.
		-- One time per chuunk. Chunk size could generally depend on available RAM
		-- for temporary storage.

	end Generic_Raster;

end Video.Images;
