with Interfaces;
use Interfaces;
--
-- Color management
--
package Video.Colors with Pure is

	Color_Component_Bits : constant := 8;
	type Color_Component is private;
	-- Universal definition for color component (like R, G, B channels or Alpha)
	-- defined to cover the best color resolution required (e. g. 8 bit in true color systems)

	subtype Opacity is Color_Component;

	Opaque      : constant Opacity;
	Transparent : constant Opacity;

	subtype Color_Component_1_Bit is Integer range 0 .. 1;
	subtype Color_Component_4_Bit is Integer range 0 .. 2**4 - 1;
	subtype Color_Component_5_Bit is Integer range 0 .. 2**5 - 1;
	subtype Color_Component_6_Bit is Integer range 0 .. 2**6 - 1;
	subtype Color_Component_8_Bit is Integer range 0 .. 2**8 - 1;
	-- Integer representation of a color component in range 0 .. 255;

	function From_1_Bit (Value : Color_Component_1_Bit) return Color_Component with Inline;

	function From_4_Bit (Value : Color_Component_4_Bit) return Color_Component with Inline;

	function From_5_Bit (Value : Color_Component_5_Bit) return Color_Component with Inline;

	function From_6_Bit (Value : Color_Component_6_Bit) return Color_Component with Inline;

	function From_8_Bit (Value : Color_Component_8_Bit) return Color_Component with Inline;
	-- Convert from integer reptesentation

	function To_1_Bit (Value : Color_Component) return Color_Component_1_Bit with Inline;

	function To_4_Bit (Value : Color_Component) return Color_Component_4_Bit with Inline;

	function To_5_Bit (Value : Color_Component) return Color_Component_5_Bit with Inline;

	function To_6_Bit (Value : Color_Component) return Color_Component_6_Bit with Inline;

	function To_8_Bit (Value : Color_Component) return Color_Component_8_Bit with Inline;
	-- Comvert to integer representation

	type Color is record
		R : Color_Component;
		G : Color_Component;
		B : Color_Component;
		A : Opacity := Opaque;
	end record;

	function RGB_8 (
		R, G, B : Color_Component_8_Bit;
		A       : Color_Component_8_Bit := Color_Component_8_Bit'Last)
		return Color with Inline;
	-- Define color by R, G, B and alpha 8 bit components

	function Hex_6 (V : Unsigned_32) return Color is
	(RGB_8 (B => Integer (V and 16#FF#),
		G => Integer (Shift_Right (V, 8) and 16#FF#),
		R => Integer (Shift_Right (V, 16) and 16#FF#)));
	-- Define color by 6-digit hex value

private
	type Color_Component is range 0 .. 2**Color_Component_Bits - 1;

	Opaque      : constant Opacity := Color_Component'Last;
	Transparent : constant Opacity := Color_Component'First;

end Video.Colors;