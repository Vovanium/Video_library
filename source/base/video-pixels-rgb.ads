with Video.Colors;
use  Video.Colors;

--
-- RGB rasters
--
package Video.Pixels.RGB is

	-- Pixel formats
	--  1 1 1 1 1 1
	--  5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
	-- +---------+-----------+---------+
	-- |    R    |     G     |    B    | RGB565 (aka RGB16)
	-- +---------+-----------+---------+
	--  2 2 2 2 1 1 1 1 1 1 1 1 1 1
	--  3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
	-- +---------------+---------------+---------------+
	-- |       R       |       G       |       B       | RGB888 (aka RGB24)
	-- +---------------+---------------+---------------+

	-- RGB565

	type RGB565_Pixel is record
		R : Integer range 0 .. 2**5 - 1;
		G : Integer range 0 .. 2**6 - 1;
		B : Integer range 0 .. 2**5 - 1;
	end record with Size => 16;
	for RGB565_Pixel use record
		R at 0 range 11 .. 15;
		G at 0 range  5 .. 10;
		B at 0 range  0 ..  4;
	end record;

	function From_Color (Color : Colors.Color) return RGB565_Pixel is (
		R => Colors.To_8_Bit (Color.R) / 8,
		G => Colors.To_8_Bit (Color.G) / 4,
		B => Colors.To_8_Bit (Color.B) / 8) with Inline;

	function To_Color (Pixel : RGB565_Pixel) return Color is (
		R => From_5_Bit (Pixel.R),
		G => From_6_Bit (Pixel.G),
		B => From_5_Bit (Pixel.B),
		others => <>) with Inline;

	type RGB565_Array is array (Integer range <>, Integer range <>) of RGB565_Pixel;

	-- RGB888

	type RGB888_Pixel is record
		R : Integer range 0 .. 2**8 - 1;
		G : Integer range 0 .. 2**8 - 1;
		B : Integer range 0 .. 2**8 - 1;
	end record with Size => 24;
	for RGB888_Pixel use record
		R at 0 range 16 .. 23;
		G at 0 range  8 .. 15;
		B at 0 range  0 ..  7;
	end record;

	function From_Color (Color : Colors.Color) return RGB888_Pixel is (
		R => Colors.To_8_Bit (Color.R),
		G => Colors.To_8_Bit (Color.G),
		B => Colors.To_8_Bit (Color.B)) with Inline;

	function To_Color (Pixel : RGB888_Pixel) return Color
	is (RGB_8 (R => Pixel.R, G => Pixel.G, B => Pixel.B)) with Inline;

	type RGB888_Array is array (Integer range <>, Integer range <>) of RGB888_Pixel;

end Video.Pixels.RGB;
