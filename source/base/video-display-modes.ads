--
-- Video display modes: geomtries and timings
--
package Video.Display.Modes with Pure is

	--                   Horiz. period
	--   :<-------------------------------------->:
	--   :H.Sync                                  :
	--  >:--:<                                    :
	--   :  :H. back                              :
	--   :  :porch       Horiz. active            :
	--   :  :<-->:<------------------------->:    :           V. sync
	--   :__:____:___________________________:____:..............V.......
	--   |  :____:___________Vsync___________:____|..............|.    ^
	--   |  |    :       Blanking area       :    |V. back porch ^     |
	--   |  |    :___________________________:....|..............V.    |
	--   |H |    |  ________Border_________  |    |        ^           |
	--   |S |    | |                       | |    |        |           |
	--   |y |    | |                       | |    |        |           |
	--   |n |    | |        Graphic        | |    |  Vert. |     Vert. |
	--   |v |    | |         Area          | |    | Active |    period |
	--   |  |    | |                       | |    |        |           |
	--   |  |    | |                       | |    |        |           |
	--   |  |    | |_______________________| |    |        |           |
	--   |  |    |___________________________|....|........V.          |
	--   |  |                                     |                    |
	--   |__|_____________________________________|....................V.
	--
	-- Front porch = Period - Active - Back porch - Back porch
	-- (for both vertical and horizontal)

	type Frequency_Hz is range 0 .. 2_000_000_000;

	type Polarity is (
		Active_High,
		Active_Low
	);

	type Video_Geometry is record
		Pixel_Clock_Frequency_Hz : Frequency_Hz; -- Value in Hertz
		Horizontal_Period        : Integer; -- Value in pixel clock ticks
		Horizontal_Sync          : Integer;
		Horizontal_Back_Porch    : Integer; -- Timing between end of HSync and start of active area
		Horizontal_Active        : Integer;
		Vertical_Period          : Integer; -- Vertical scan period in value in lines
		Vertical_Sync            : Integer;
		Vertical_Back_Porch      : Integer;
		Vertical_Active          : Integer;
		Horizontal_Sync_Polarity : Polarity;
		Vertical_Sync_Polarity   : Polarity;
		Composite_Sync_Polarity  : Polarity;
	end record;

end Video.Display.Modes;
