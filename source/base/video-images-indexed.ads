with Video.Pixels.Indexed, Video.Colors.Palettes, Video.Colors.Maps;
use  Video.Pixels.Indexed, Video.Colors.Palettes, Video.Colors.Maps;
with Video.Colors.Maps.Generic_Fixed;
with Video.Colors.Maps.Shared_Palettes;

package Video.Images.Indexed is

	type Indexed_Image is limited interface and Raster_Image and Constant_Color_Map;

	function Pixel_Index (
		Source : Indexed_Image;
		A      : Point)
		return   Color_Index is abstract;
	-- A pixel index value, an untyped way to read it

	-- note: Function is named `Pixel_Index` to avoid confusing with `Pixel` group
	--       As it returns integer type it can break type safety

	--

	generic
		type Pixel_Type is mod <>;
		type Array_Type is array (Integer range <>, Integer range <>) of Pixel_Type;
	package Indexed_Base is

		package Raster is new Generic_Raster (
			Pixel_Type => Pixel_Type,
			Array_Type => Array_Type,
			Parent     => Indexed_Image);

		subtype Image is Raster.Image;

		package Fixed_Color_Maps is new Colors.Maps.Generic_Fixed (0, Pixel_Type'Modulus - 1);

		generic
			type Color_Map is new Video.Colors.Maps.Constant_Color_Map with private;
		package Images_with_Color_Map is
			type Indexed_Image_Base
			is abstract new Color_Map and Raster.Image with null record;

			function Map_Color (Map : Indexed_Image_Base; Pixel : Pixel_Type) return Color
			is (Map.Map_Color (Color_Index (Pixel))) with Inline;

			overriding function Pixel_Index (
				Source : Indexed_Image_Base;
				A      : Point)
				return   Color_Index
			is (Color_Index (Pixel_Type'(Raster.Image'Class (Source).Pixel (A))));

			function Pixel (
				Source : Indexed_Image_Base;
				A      : Point)
				return   Color
			is (Source.Map_Color (Color_Index (Pixel_Type'(Raster.Image'Class (Source).Pixel (A)))));

		end Images_with_Color_Map;

		package Own is new Images_with_Color_Map (
			Color_Map => Fixed_Color_Maps.Fixed_Color_Map);

		package Shared is new Images_with_Color_Map (
			Color_Map => Colors.Maps.Shared_Palettes.Constant_Shared_Palette);

	end Indexed_Base;

	package Index_1 is new Indexed_Base (
		Pixel_Type => Index_1_Pixel,
		Array_Type => Index_1_Array);

	package Index_4 is new Indexed_Base (
		Pixel_Type => Index_4_Pixel,
		Array_Type => Index_4_Array);

	package Index_8 is new Indexed_Base (
		Pixel_Type => Index_8_Pixel,
		Array_Type => Index_8_Array);

end Video.Images.Indexed;
