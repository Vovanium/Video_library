-- A version of fixed map to use where unconstrained type is not suitable

generic
	First, Last : in Color_Index;
package Video.Colors.Maps.Generic_Fixed is
	type Fixed_Color_Map is new Color_Map with record
		Colors : Palette (First .. Last);
	end record;
	-- A simple wrapper for Palette type

	-- note: not hiding implementation details so one can statically initialize them

	overriding function First_Color (Map : Fixed_Color_Map) return Color_Index is (First);

	overriding function Last_Color (Map : Fixed_Color_Map) return Color_Index is (Last);

	overriding function Map_Color (Map : Fixed_Color_Map; Index : Color_Index) return Color is (Map.Colors (Index));

	overriding function To_Palette (Map : Fixed_Color_Map) return Palette is (Map.Colors);

	overriding procedure Replace_Color (
		Map   : in out Fixed_Color_Map;
		Index : in     Color_Index;
		Value : in     Color) with Inline;

	overriding procedure From_Palette (
		Map    : in out Fixed_Color_Map;
		Source : in     Palette);

end Video.Colors.Maps.Generic_Fixed;
