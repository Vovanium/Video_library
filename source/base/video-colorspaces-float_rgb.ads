package Video.Colorspaces.Float_RGB with Pure is

	subtype Color_Component is Float;

	subtype Opacity is Color_Component;

	Opaque      : constant Opacity := 1.0;
	Transparent : constant Opacity := 0.0;

	type Color is record
		R : Color_Component;
		G : Color_Component;
		B : Color_Component;
		A : Opacity := Opaque;
	end record;

	subtype Linear_Color is Color;

	package sRGB is
		function To_sRGB (C_L : Float) return Float;

		function From_sRGB (C_S : Float) return Float;

		subtype sRGB_Color is Color;

		function From_Linear (C : Linear_Color) return sRGB_Color
		is (R => To_sRGB (C.R), G => To_sRGB (C.G), B => To_sRGB (C.B), A => C.A);

		function To_Linear (C : sRGB_Color) return Linear_Color
		is (R => From_sRGB (C.R), G => From_sRGB (C.G), B => From_sRGB (C.B), A => C.A);
	end sRGB;

	generic
		Gamma : in Float;
	package With_Gamma  is
		Inverse_Gamma : constant Float := 1.0 / Gamma;

		subtype Gamma_Color is Color;

		function From_Linear (C : Linear_Color) return Gamma_Color;

		function To_Linear (C : Gamma_Color) return Linear_Color;
	end  With_Gamma;

end Video.Colorspaces.Float_RGB;