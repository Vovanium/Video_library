package body Video.IO.Writers is

	procedure Write (Writer : in out Base_Writer; Data : in Unsigned_8) is
	begin
		Unsigned_8'Write (Writer.Stream, Data);
		Writer.Pos := Writer.Pos + 1;
	end Write;

	procedure Write (Writer : in out Base_Writer; Data : in Unsigned_8_Array) is
	begin
		Unsigned_8_Array'Write (Writer.Stream, Data);
		Writer.Pos := Writer.Pos + Data'Length;
	end Write;

end Video.IO.Writers;
