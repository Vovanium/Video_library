with Video.Pixels.ARGB;

package Video.IO.BMP.Arrays.ARGB is

	procedure Output (
		Stream  : access Ada.Streams.Root_Stream_Type'Class;
		Data    : in     Video.Pixels.ARGB.ARGB8888_Array;
		Palette : in     Colors.Palettes.Palette := Colors.Palettes.Empty_Palette);

end Video.IO.BMP.Arrays.ARGB;
