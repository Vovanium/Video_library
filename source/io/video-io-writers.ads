with Interfaces;
use  Interfaces;

package Video.IO.Writers is
	type Unsigned_8_Array is array (Integer range <>) of Unsigned_8;

	type Base_Writer is tagged limited record
		Stream : access Ada.Streams.Root_Stream_Type'Class;
		Pos    : Integer := 0;
	end record;

	procedure Write (Writer : in out Base_Writer; Data : in Unsigned_8);

	procedure Write (Writer : in out Base_Writer; Data : in Unsigned_8_Array);

end Video.IO.Writers;
