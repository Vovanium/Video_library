with Interfaces;
use Interfaces;

package body Video.IO.BMP.Arrays.RGB is

	type Buf_Array is array (Natural range <>) of Unsigned_8;

	procedure RGB888_Write_Row (
		Stream : access Ada.Streams.Root_Stream_Type'Class;
		Data   : Video.Pixels.RGB.RGB888_Array;
		Y      : Integer;
		Bytes  : Positive)
	is
		Pos  : Natural := 0;
		Buf  : Buf_Array (0 .. Bytes - 1) := (others => 0);
	begin
		for X in Data'Range (2) loop
			Buf (Pos)     := Unsigned_8 (Data (Y, X).B);
			Buf (Pos + 1) := Unsigned_8 (Data (Y, X).G);
			Buf (Pos + 2) := Unsigned_8 (Data (Y, X).R);
			Pos := Pos + 3;
		end loop;
		Buf_Array'Write (Stream, Buf);
	end;

	-- Note: we need (nominally) pixel transcoding because bit order may not match

	procedure RGB888_Output is new Generic_Output (
		Pixel_Type     => Video.Pixels.RGB.RGB888_Pixel,
		Array_Type     => Video.Pixels.RGB.RGB888_Array,
		Bits_Per_Pixel => 24,
		Write_Row      => RGB888_Write_Row);

	procedure Output (
		Stream  : access Ada.Streams.Root_Stream_Type'Class;
		Data    : in     Video.Pixels.RGB.RGB888_Array;
		Palette : in     Colors.Palettes.Palette := Colors.Palettes.Empty_Palette)
	renames RGB888_Output;

end Video.IO.BMP.Arrays.RGB;
