with Ada.Unchecked_Conversion;

package body Video.IO.Readers.LE is

	procedure Read (Reader : in out LE_Reader; Data : out Unsigned_16) is
		Buf : Unsigned_8_Array (0 .. 1);
	begin
		Reader.Read (Buf);
		Data :=       Unsigned_16 (Buf (0))
		+ Shift_Left (Unsigned_16 (Buf (1)), 8);
	end Read;

	procedure Read (Reader : in out LE_Reader; Data : out Unsigned_32) is
		Buf : Unsigned_8_Array (0 .. 3);
	begin
		Reader.Read (Buf);
		Data :=       Unsigned_32 (Buf (0))
		+ Shift_Left (Unsigned_32 (Buf (1)), 8)
		+ Shift_Left (Unsigned_32 (Buf (2)), 16)
		+ Shift_Left (Unsigned_32 (Buf (3)), 24);
	end Read;

	function To_Integer_32 is new Ada.Unchecked_Conversion (Source => Unsigned_32, Target => Integer_32);

	procedure Read (Reader : in out LE_Reader; Data : out Integer_32) is
		T : Unsigned_32;
	begin
		Reader.Read (T);
		Data := To_Integer_32 (T);
	end Read;

end Video.IO.Readers.LE;
