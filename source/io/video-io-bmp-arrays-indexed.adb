with Interfaces;
use Interfaces;

package body Video.IO.BMP.Arrays.Indexed is

	generic
		type Pixel_Type is mod <>;
		type Array_Type is array (Integer range <>, Integer range <>) of Pixel_Type;
	procedure Generic_Write_Row (
		Stream : access Ada.Streams.Root_Stream_Type'Class;
		Data   : Array_Type;
		Y      : Integer;
		Bytes  : Positive);

	procedure Generic_Write_Row (
		Stream : access Ada.Streams.Root_Stream_Type'Class;
		Data   : Array_Type;
		Y      : Integer;
		Bytes  : Positive)
	is
		type Buf_Array is array (Natural range <>) of Unsigned_8;
		Bits : Integer := 8;
		Pos  : Natural := 0;
		Buf  : Buf_Array (0 .. Bytes - 1) := (others => 0);
	begin
		for X in Data'Range (2) loop
			Bits := Bits - Pixel_Type'Size;
			Buf (Pos) := Buf (Pos) or Shift_Left (Unsigned_8 (Data (Y, X)), Bits);
			if Bits <= 0 then
				Bits := 8;
				Pos := Pos + 1;
			end if;
		end loop;
		Buf_Array'Write (Stream, Buf);

	end Generic_Write_Row;

	procedure Index_1_Write_Row is new Generic_Write_Row (
		Pixel_Type  => Video.Pixels.Indexed.Index_1_Pixel,
		Array_Type => Video.Pixels.Indexed.Index_1_Array);


	procedure Index_1_Output is new Generic_Output (
		Pixel_Type  => Video.Pixels.Indexed.Index_1_Pixel,
		Array_Type => Video.Pixels.Indexed.Index_1_Array,
		Bits_Per_Pixel => 1,
		Write_Row => Index_1_Write_Row);

	procedure Output (
		Stream  : access Ada.Streams.Root_Stream_Type'Class;
		Data    : in     Video.Pixels.Indexed.Index_1_Array;
		Palette : in     Colors.Palettes.Palette := Colors.Palettes.Empty_Palette)
	renames Index_1_Output;

end Video.IO.BMP.Arrays.Indexed;
