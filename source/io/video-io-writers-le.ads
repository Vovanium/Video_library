package Video.IO.Writers.LE is
	type LE_Writer is new Base_Writer with null record;

	procedure Write (Writer : in out LE_Writer; Data : in Unsigned_16);

	procedure Write (Writer : in out LE_Writer; Data : in Unsigned_32);

	procedure Write (Writer : in out LE_Writer; Data : in Integer_32);

end Video.IO.Writers.LE;
