package body Video.IO.Readers is

	procedure Read (Reader : in out Base_Reader; Data : out Unsigned_8) is
	begin
		Unsigned_8'Read (Reader.Stream, Data);
		Reader.Pos := Reader.Pos + 1;
	end Read;

	procedure Read (Reader : in out Base_Reader; Data : out Unsigned_8_Array) is
	begin
		Unsigned_8_Array'Read (Reader.Stream, Data);
		Reader.Pos := Reader.Pos + Data'Length;
	end Read;

end Video.IO.Readers;
