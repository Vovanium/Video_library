with Video.Pixels.RGB;

package Video.IO.BMP.Arrays.RGB is

	procedure Output (
		Stream  : access Ada.Streams.Root_Stream_Type'Class;
		Data    : in     Video.Pixels.RGB.RGB888_Array;
		Palette : in     Colors.Palettes.Palette := Colors.Palettes.Empty_Palette);

end Video.IO.BMP.Arrays.RGB;
