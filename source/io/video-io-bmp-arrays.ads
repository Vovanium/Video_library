with Video.Colors.Palettes;
with Video.Pixels.Formats;

package Video.IO.BMP.Arrays is
	generic
		type Pixel_Type is private;
		type Array_Type is array (Integer range <>, Integer range <>) of Pixel_Type;
		Bits_Per_Pixel : Positive;
		with procedure Write_Row (
			Stream : access Ada.Streams.Root_Stream_Type'Class;
			Data   : Array_Type;
			Y      : Integer;
			Bytes  : Positive);
		Format : Video.Pixels.Formats.Pixel_Format
			:= Video.Pixels.Formats.No_Format;
	procedure Generic_Output (
		Stream  : access Ada.Streams.Root_Stream_Type'Class;
		Data    : in     Array_Type;
		Palette : in     Colors.Palettes.Palette := Colors.Palettes.Empty_Palette);
end Video.IO.BMP.Arrays;
