with Video.Pixels.Indexed;

package Video.IO.BMP.Arrays.Indexed is

	procedure Output (
		Stream  : access Ada.Streams.Root_Stream_Type'Class;
		Data    : in     Video.Pixels.Indexed.Index_1_Array;
		Palette : in     Colors.Palettes.Palette := Colors.Palettes.Empty_Palette);

end Video.IO.BMP.Arrays.Indexed;
