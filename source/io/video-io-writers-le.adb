package body Video.IO.Writers.LE is

	procedure Write (Writer : in out LE_Writer; Data : in Unsigned_16) is
		Buf : Unsigned_8_Array (0 .. 1);
	begin
		Buf (0) := Unsigned_8 (Data                  and (2**8 - 1));
		Buf (1) := Unsigned_8 (Shift_Right (Data, 8) and (2**8 - 1));
		Writer.Write (Buf);
	end Write;

	procedure Write (Writer : in out LE_Writer; Data : in Unsigned_32) is
		Buf : Unsigned_8_Array (0 .. 3);
	begin
		Buf (0) := Unsigned_8 (Data                   and (2**8 - 1));
		Buf (1) := Unsigned_8 (Shift_Right (Data,  8) and (2**8 - 1));
		Buf (2) := Unsigned_8 (Shift_Right (Data, 16) and (2**8 - 1));
		Buf (3) := Unsigned_8 (Shift_Right (Data, 24) and (2**8 - 1));
		Writer.Write (Buf);
	end Write;

	procedure Write (Writer : in out LE_Writer; Data : in Integer_32) is
	begin
		Writer.Write (Unsigned_32 (Data));
	end Write;

end Video.IO.Writers.LE;
