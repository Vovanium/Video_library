with Interfaces;
use  Interfaces;

with Video.Pixels.Formats;
use type Video.Pixels.Formats.Color_Mode;
use type Video.Pixels.Formats.Component_Offset;

with Video.IO.Writers.LE;

package body Video.IO.BMP.Arrays is

	type Unsigned_8_Array is array (Integer range <>) of Unsigned_8;

	procedure Generic_Output (
		Stream  : access Ada.Streams.Root_Stream_Type'Class;
		Data    : in     Array_Type;
		Palette : in     Colors.Palettes.Palette := Colors.Palettes.Empty_Palette)
	is
		BF_Size : constant Positive := 14;
		BI_Size : constant Positive := (
			if Format.Mode /= Video.Pixels.Formats.ARGB then 56
			else 40);
		CT_Size : constant Natural := Palette'Length * 4;

		BM_Offset : constant Natural := BF_Size + BI_Size + CT_Size;
		Row_Size_Prec : constant Natural := Bits_Per_Pixel * Data'Length (2) / 8;
		Row_Size : constant Natural := (Row_Size_Prec + 3) / 4 * 4; -- round up
		BM_Size : constant Natural := Data'Length (1) * Row_Size;

		File_Size : constant Positive := BM_Offset + BM_Size;

		Writer : Writers.LE.LE_Writer := (Stream => Stream, Pos => 0);

		function To_Mask (R : Video.Pixels.Formats.Component_Range) return Unsigned_32
		is (Shift_Left (Unsigned_32'(1), Natural (R.Last + 1))
		- Shift_Left (Unsigned_32'(1), Natural (R.First)));
	begin
		-- File Header
		Writer.Write (Unsigned_16 (Character'Pos ('B') + Character'Pos ('M') * 256));
		Writer.Write (Unsigned_32 (File_Size));
		Writer.Write (Unsigned_32 (0)); -- Reserved
		Writer.Write (Unsigned_32 (BM_Offset));

		-- DIB Header
		Writer.Write (Unsigned_32 (BI_Size));
		if BI_Size >= 16 then
			Writer.Write (Integer_32 (Data'Length (2)));
			Writer.Write (Integer_32 (Data'Length (1)));
			Writer.Write (Unsigned_16 (1)); -- Number of planes
			Writer.Write (Unsigned_16 (Bits_Per_Pixel));
		end if;

		if BI_Size >= 40 then
			Writer.Write (Unsigned_32 (0)); -- BI_RGB
			Writer.Write (Unsigned_32 (BM_Size));
			Writer.Write (Unsigned_32 (1000)); -- Horizontal resolution
			Writer.Write (Unsigned_32 (1000)); -- Vertical resolution
			Writer.Write (Unsigned_32 (Palette'Length));
			Writer.Write (Unsigned_32 (0));    -- Important colors (0 means all)
		end if;

		if BI_Size >= 52 then
			if Format.Mode = Video.Pixels.Formats.ARGB then
				Writer.Write (To_Mask (Format.R));
				Writer.Write (To_Mask (Format.G));
				Writer.Write (To_Mask (Format.B));
			else
				Writer.Write (Unsigned_32 (0));
				Writer.Write (Unsigned_32 (0));
				Writer.Write (Unsigned_32 (0));
			end if;
		end if;

		if BI_Size >= 56 then
			if Format.Mode = Video.Pixels.Formats.ARGB then
				Writer.Write (To_Mask (Format.A));
			else
				Writer.Write (Unsigned_32 (0));
			end if;
		end if;

		-- Color Table
		if BI_Size >= 16 then
			for I in Palette'Range loop
				Writer.Write (Unsigned_8 (Colors.To_8_Bit (Palette (I).R)));
				Writer.Write (Unsigned_8 (Colors.To_8_Bit (Palette (I).G)));
				Writer.Write (Unsigned_8 (Colors.To_8_Bit (Palette (I).B)));
				Writer.Write (Unsigned_8 (Colors.To_8_Bit (Palette (I).A)));
			end loop;
		end if;

		pragma Assert (Writer.Pos = BM_Offset);

		-- Pixels
		for Y in reverse Data'Range (1) loop
			Write_Row (Stream, Data, Y, Row_Size);
			Writer.Pos := Writer.Pos + Row_Size;
		end loop;

		pragma Assert (Writer.Pos = File_Size);
	end Generic_Output;

end Video.IO.BMP.Arrays;
