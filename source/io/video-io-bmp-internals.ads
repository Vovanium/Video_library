package Video.IO.BMP.Internals with Pure is

	package Format is
		subtype Header_Tag is String (1 .. 2);

		type Unsigned_8  is mod 2**8  with Size => 8;
		type Unsigned_16 is mod 2**16 with Size => 16;
		type Unsigned_32 is mod 2**32 with Size => 32;
		type Signed_32   is range -2**31 .. 2**31 - 1 with Size => 32;

		type File_Header is record
			Tag           : Header_Tag := "BM"; -- BM, BA, CI, CP, IC or PT is suitable
			Size          : Unsigned_32; -- Size of the file in bytes
			Reserved_1,
			Reserved_2    : Unsigned_16; -- Generally unused
			Bitmap_Offset : Unsigned_32; -- Bitmap data offset from the beginning of file
		end record;

		type Compression_Method is new Unsigned_32;

		BI_RGB            : constant Compression_Method := 0;
		BI_RLE8           : constant Compression_Method := 1;
		BI_RLE4           : constant Compression_Method := 2;
		BI_Bitfileds      : constant Compression_Method := 3;
		BI_JPEG           : constant Compression_Method := 4;
		BI_PNG            : constant Compression_Method := 5;
		BI_AlphaBitfields : constant Compression_Method := 6;
		BI_CMYK           : constant Compression_Method := 11;
		BI_CMYKRLE8       : constant Compression_Method := 12;
		BI_CMYKRLE4       : constant Compression_Method := 13;

		subtype Bitmap_Info_Size is Unsigned_32;

		-- Next headers are without Size field

		-- BI_Size = 12: Windows 2.0, OS/2 1.x
		type Core_Header is record
			Width,                         -- Pixel width of the image
			Height    : Unsigned_16;       -- Pixel height of the image (Windows 2.0 treats them as signed)
			Planes    : Unsigned_16 := 1;  -- Number of planes (must be one)
			Bit_Count : Unsigned_16;       -- Bits per pixel (1, 2, 4 or 8)
		end record with Pack;

		type Color_Space is new Unsigned_32;
		LCS_Calibrated_RGB : constant Color_Space := 0;

		type Fixed_2_30 is delta 2.0**(-30) range -2.0 .. 2.0 with Size => 32;

		type CIE_XYZ is record
			X, Y, Z : Fixed_2_30 := 0.0;
		end record;

		type CIE_XYZ_Triple is record
			Red   : CIE_XYZ := (0.64, 0.33, 0.03);
			Green : CIE_XYZ := (0.30, 0.60, 0.10);
			Blue  : CIE_XYZ := (0.15, 0.06, 0.79);
		end record with Pack;

		type UFixed_16_16 is delta 2.0**(-16) range 0.0 .. 2.0**16 with Size => 32;

		type Rendering_Intent is new Unsigned_32;
		RI_Perceptual : constant Rendering_Intent := 4;

		-- BI_Size = 16: OS/2 2.x
		-- BI_Size = 40: Windows 3.1, NT
		type Info_Header is record
			Width,                                            -- Pixel width
			Height            : Signed_32;                    -- Pixel height (Negative for top-down order)
			Planes            : Unsigned_16        := 1;      -- Number of color planes (must be one)
			Bit_Count         : Unsigned_16;                  -- Bits per pixel
			-- when BI_Size = 16 data stops here
			Compression       : Compression_Method := BI_RGB; -- Image data format
			Size_Image        : Unsigned_32        := 0;      -- Size of raw data in bytes
			X_Pels_Per_Meter,                                 -- Horizontal resolution
			Y_Pels_Per_Meter  : Signed_32          := 0;      -- Vertical resolution
			Colors_Used       : Unsigned_32        := 0;      -- Colors in palette (0 for 2**bits)
			Colors_Important  : Unsigned_32        := 0;      -- Importrand colors (0 for All)
			-- when BI_Size = 40 data stops here
			Red_Mask,                                         -- Mask for red component
			Green_Mask,                                       -- Mask for green component
			Blue_Mask         : Unsigned_32        := 0;      -- Mask for blue component
			-- when BI_Size = 52 data stops here
			Alpha_Mask        : Unsigned_32        := 0;      -- Mask for alpha component
			-- when BI_Size = 56 data stops here
			CS_Type           : Color_Space        := LCS_Calibrated_RGB;
			Endpoints         : CIE_XYZ_Triple;
			Gamma_Red,
			Gamma_Green,
			Gamma_Blue        : UFixed_16_16       := 2.2;
			-- when BI_Size = 108 data stops here
			Intent            : Rendering_Intent   := RI_Perceptual;
			Profile_Data      : Unsigned_32        := 0;
			Profile_Size      : Unsigned_32        := 0;
			Reserved          : Unsigned_32        := 0;
		end record with Pack;

		type Resolution_Units is new Unsigned_16;
		BI_PPM : constant Resolution_Units := 0; -- resolution is in pixels per metre

		type Fill_Direction is new Unsigned_16;
		BI_LR_BU : constant Fill_Direction := 0; -- bitmap fill direction: left-to-right then bottom-up

		type Halftoning_Algorithm is new Unsigned_16;
		BI_HT_None         : constant Halftoning_Algorithm := 0; -- No halftoning
		BI_HT_PANDA        : constant Halftoning_Algorithm := 1; -- PANDA
		BI_HT_Super_Circle : constant Halftoning_Algorithm := 2; -- Super-circle

		type Color_Encoding is new Unsigned_16; -- Palette color encoding
		BI_CE_RGB : constant Color_Encoding := 0; -- RGB

		-- BI_Size = 64 : OS/2 2.x
		type OS2_Header is record
			Width,                                            -- Pixel width
			Height            : Unsigned_32;                  -- Pixel height (Negative for top-down order)
			Planes            : Unsigned_16        := 1;      -- Number of color planes (must be one)
			Bit_Count         : Unsigned_16;                  -- Bits per pixel
			Compression       : Compression_Method := BI_RGB; -- Image data format
			Size_Image        : Unsigned_32        := 0;      -- Size of raw data in bytes
			X_Resolution,                                     -- Horizontal resolution
			Y_Resolution      : Signed_32          := 0;      -- Vertical resolution
			Colors_Used       : Unsigned_32        := 0;      -- Colors in palette (0 for 2**bits)
			Colors_Important  : Unsigned_32        := 0;      -- Importrand colors (0 for All)
			Units             : Resolution_Units   := BI_PPM; -- Resolution unit
			Padding           : Unsigned_16        := 0;      -- unused
			Direction         : Fill_Direction     := BI_LR_BU;
			Halftoning        : Halftoning_Algorithm := BI_HT_None;
			Parameter_1,                                      -- Halftoning parameter 1
			Parameter_2       : Unsigned_32        := 0;      -- Halftoning parameter 2
			Encoding          : Color_Encoding     := BI_CE_RGB;
			Application       : Unsigned_32        := 0;      -- Application specific parameter
		end record with Pack;
	end Format;

end Video.IO.BMP.Internals;
