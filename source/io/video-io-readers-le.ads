package Video.IO.Readers.LE is
	type LE_Reader is new Base_Reader with null record;

	procedure Read (Reader : in out LE_Reader; Data : out Unsigned_16);

	procedure Read (Reader : in out LE_Reader; Data : out Unsigned_32);

	procedure Read (Reader : in out LE_Reader; Data : out Integer_32);

end Video.IO.Readers.LE;
