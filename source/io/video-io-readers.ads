with Interfaces;
use  Interfaces;

package Video.IO.Readers is
	type Unsigned_8_Array is array (Integer range <>) of Unsigned_8;

	type Base_Reader is tagged limited record
		Stream : access Ada.Streams.Root_Stream_Type'Class;
		Pos    : Integer := 0;
	end record;

	procedure Read (Reader : in out Base_Reader; Data : out Unsigned_8);

	procedure Read (Reader : in out Base_Reader; Data : out Unsigned_8_Array);

end Video.IO.Readers;
