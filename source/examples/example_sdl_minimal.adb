with Video.Pixels.RGB.Renderers;
with Video.SDL_Minimal;
with Video.SDL_Minimal.Video;
with Video.SDL_Minimal.Pixels;
with Video.SDL_Minimal.Render;
with Video.SDL_Minimal.Events;
use type Video.SDL_Minimal.Events.EventType;
with Interfaces.C;
use  Interfaces;
use type Interfaces.C.int;
with Example_Scene;

procedure Example_SDL_Minimal
is
	Screen_Width  : constant := 640;
	Screen_Height : constant := 480;
	Screen        : aliased Video.Pixels.RGB.RGB565_Array := (0 .. Screen_Height - 1 =>  (0 .. Screen_Width - 1 => <>));
	Rend          : Video.Pixels.RGB.Renderers.RGB565_Renderers.Renderer (Screen'Access);
	Wnd           : Video.SDL_Minimal.Video.Window_Ptr;
	Wnd_Rend      : Video.SDL_Minimal.Render.Renderer_Ptr;
	Tx            : Video.SDL_Minimal.Render.Texture_Ptr;
	Ev            : Video.SDL_Minimal.Events.Event;

	procedure Catch (Err : C.int) is
	begin
		if Err /= 0 then
			raise Program_Error;
		end if;
	end;
begin
	Wnd := Video.SDL_Minimal.Video.CreateWindow (
		Title    => "Hello World",
		X        => Video.SDL_Minimal.Video.WindowPos_Centered,
		Y        => Video.SDL_Minimal.Video.WindowPos_Centered,
		W        => Screen_Width,
		H        => Screen_Height,
		Flags    => 0);

	Wnd_Rend := Video.SDL_Minimal.Render.CreateRenderer (Wnd);

	Tx := Video.SDL_Minimal.Render.CreateTexture (Wnd_Rend,
		Video.SDL_Minimal.Pixels.PixelFormat_RGB565,
		Video.SDL_Minimal.Render.TextureAccess_Static,
		Screen_Width, Screen_Height);

	Example_Scene (Rend);

	Catch (Video.SDL_Minimal.Render.UpdateTexture (Tx, null, Screen'Address, Screen_Width * 2));

	Catch (Video.SDL_Minimal.Render.RenderClear (Wnd_Rend));
	Catch (Video.SDL_Minimal.Render.RenderCopy (Wnd_Rend, Tx, null, null));
	Video.SDL_Minimal.Render.RenderPresent (Wnd_Rend);

	Wait : loop
		while Video.SDL_Minimal.Events.PollEvent (Ev) /= 0 loop
			exit Wait when Ev.Common.Kind = Video.SDL_Minimal.Events.Quit;
		end loop;
	end loop Wait;

	Video.SDL_Minimal.Render.DestroyTexture (Tx);

	Video.SDL_Minimal.Render.DestroyRenderer (Wnd_Rend);

	Video.SDL_Minimal.Video.DestroyWindow (Wnd);

end Example_SDL_Minimal;