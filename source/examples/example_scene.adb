with Video.Colors;
with Video.Integer_Geometry;
use  Video.Integer_Geometry;

procedure Example_Scene (R : in out Video.Renderers.Renderer'Class) is
	BB : constant Box := R.Bounding_Box;
	BBP : constant Box := BB - R.Pen_Extent;
	Center : constant Point := Video.Integer_Geometry.Center (BB);
begin
	R.Set_Color (Video.Colors.RGB_8 (0, 0, 0));
	R.Clear;

	R.Set_Color (Video.Colors.Hex_6 (16#FACE8D#));
	R.Fill_Rectangle (((200, 400), (200, 300)));

	for Y in 0 .. 255 loop
		for X in 0 .. 255 loop
			R.Set_Color (Video.Colors.RGB_8 (X, Y, 255 - (X + Y) / 2));
			R.Plot ((X, Y));
		end loop;
	end loop;

	R.Set_Color (Video.Colors.Hex_6 (16#FFFFFF#));
	for X in BBP.X.First / 8 .. (BBP.X.Last) / 8 loop
		R.Line (Center, (X * 8, BBP.Y.Last));
	end loop;

	R.Set_Color (Video.Colors.Hex_6 (16#FF00FF#));
	for X in 0 .. 10 loop
		R.Circle ((Center.X, Center.Y), X * 10);
	end loop;

	R.Set_Color (Video.Colors.Hex_6 (16#00FFFF#));
	for X in 0 .. 10 loop
		R.Ellipse (((Center.X - X * 20,       Center.X + X * 20),
		            (Center.Y - 200 + X * 20, Center.Y + 200 - X * 20)));
	end loop;

end Example_Scene;
