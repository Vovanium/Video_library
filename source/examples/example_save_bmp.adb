with Video.Pixels.Indexed, Video.Pixels.RGB, Video.Colors.Palettes;
use  Video.Pixels.Indexed, Video.Pixels.RGB, Video.Colors.Palettes;
use  Video.Colors;
with Video.IO.BMP.Arrays.Indexed, Video.IO.BMP.Arrays.RGB;
use  Video.IO.BMP.Arrays.Indexed, Video.IO.BMP.Arrays.RGB;
with Ada.Streams.Stream_IO;
use  Ada.Streams.Stream_IO;

procedure Example_Save_BMP is
	Img_Array : constant Index_1_Array (0 .. 7, 0 .. 15) := (
		(0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
		(0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
		(0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0),
		(0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0),
		(1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1),
		(0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0),
		(0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0),
		(0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0));
	Img_Palette : constant Palette (0 .. 1) := (
		Hex_6 (16#909090#),
		Hex_6 (16#000000#));
	Img24_Array : RGB888_Array (0 .. 255, 0 .. 255);
	F : File_Type;
	S : Stream_Access;
begin
	Create (F, Out_File, "example.bmp");
	S := Stream (F);
	Output (S, Img_Array, Img_Palette);

	for Y in Img24_Array'Range (1) loop
		for X in Img24_Array'Range (2) loop
			Img24_Array (Y, X) := From_Color (RGB_8 (X, Y, 0));
		end loop;
	end loop;
	Close (F);
	Create (F, Out_File, "examp24.bmp");
	S := Stream (F);
	Output (S, Img24_Array);
end Example_Save_BMP;
